//
//  GameViewController.swift
//  X
//
//  Created by Kevin Gannon on 7/5/15.
//  Copyright (c) 2015 Kevin Gannon. All rights reserved.
//

import UIKit
import SpriteKit

enum LevelState {
    case Current
    case Previous
    case Next
}

protocol GameProtocol : class {
    func bombTick(from: (Int, Int), levelLost: Bool)
    
    func moveTile(from: (Int, Int), to: (Int, Int))

    func pushTile(from: (Int, Int), to: (Int, Int),
                  delay: Int, pusherDistance: Int, levelLost: Bool)
    
    func moveOut(from: (Int, Int) , direction: Direction,
                 distance: Int, levelCompleted: Bool)
    
    func moveOutPush(from: (Int, Int), direction: Direction,
                     pusherDistance: Int, delay: Int)
}

protocol SceneProtocol: class {
    func levelWon()
    func sceneLoaded()
    func isLevelCompleted(id: Int) -> Bool
    func level(state: LevelState) -> Level?
}

protocol MenuDelegate: class {
    func volumeOff()
    func volumeOn()
    func redoSelected() -> (() -> Void)!
    func rateSelected()
}

class GameController: UIViewController,
                      GameProtocol,
                      SceneProtocol,
                      GameViewDelegate {
    
    private var levelDataSource: LevelDataSource!
    private var model: GameModel!
    private var scene: GameScene!
    private var gameView: GameView!
    private var currentLevelID = 1
    private var completedLevels = Set<Int>()
    private var gameState: GameState = .Paused
    
    private let levelDimension: Int = 4
    
    override func loadView() {
        view = GameView(frame: UIScreen.mainScreen().bounds)
    }
    
    override func viewWillDisappear(animated: Bool) {
        super.viewWillDisappear(animated)
        
        removeAppStateObservers()
    }

    override func viewDidLoad() {
        super.viewDidLoad()

        gameView = view as! GameView
        gameView.delegate = self
        
        levelDataSource = LevelDataSource(
            viewWidth: gameView.bounds.width,
            dimension: levelDimension)
        
        setCurrentLevelToLastPlayed()
        setCompletedLevels()
        updateInitialGameState()
        
        let currentLevel = levelDataSource.getLevel(levelID: currentLevelID)
        scene = GameScene(size: gameView.bounds.size)
        scene.sceneDelegate = self
        gameView.presentScene(scene)
        
        model = GameModel(
            dimension: currentLevel!.dimension,
            tiles: currentLevel!.board,
            gaps: currentLevel!.gaps)
        
        model.delegate = self
        
        updatePreviousLevelNavigationOption()
        updateNextLevelNavigationOption()
    }
    
    //MARK: - SceneProtocol
    func levelWon() {
        completedLevels.insert(currentLevelID)
        updateCompletedLevels()
        
        if currentLevelID == 1 {
            markTutorialCompleted()
        }
    }
    
    func isLevelCompleted(id: Int) -> Bool {
        return completedLevels.contains(id)
    }
    
    func sceneLoaded() {
        gameView.setRedoEnabled(true)

        switch gameState {
        case .Paused, .TransitionActive, .Over, .TransitionPaused, .Completed:
            gameState = .Active
        case let .TutorialInactive(solution: s, index: i):
            scene.focusTilesWithDirection(s[i], withBomb: true) {
                self.gameState = .TutorialActive(solution: s, index: i)
            }
        default:
            return
        }
    }
    
    func level(state: LevelState) -> Level? {
        switch state {
        case .Current:
            return levelDataSource.getLevel(levelID: currentLevelID)
        case .Previous:
            return levelDataSource.getLevel(levelID: currentLevelID - 1)
        case .Next:
            return levelDataSource.getLevel(levelID: currentLevelID + 1)
        }
    }
    
    //MARK: MenuDelegate
    func volumeOff() {
        scene.pauseMusic()
        
        addAppStateObservers()
    }
    
    func volumeOn() {
        scene.playMusic()
        
        removeAppStateObservers()
    }
    
    func nextLevelSelected() {
        guard let level = level(.Next) else { return }
        
        switch gameState {
        case .Paused:
            gameState = .TransitionPaused
        default:
            break
        }
        
        currentLevelID = level.id
        updateLastLevelPlayed()
        model.resetLevel(level.board, gaps: level.gaps)
        gameView.setRedoEnabled(false)
        scene.loadNextLevel {
            self.updateNextLevelNavigationOption()
            self.updatePreviousLevelNavigationOption()
        }
        
        updatePreviousLevelNavigationOption(true)
        updateNextLevelNavigationOption(true)
    }
    
    func previousLevelSelected() {
        guard let level = level(.Previous) else { return }

        gameState = .TransitionPaused
        currentLevelID = level.id
        updateLastLevelPlayed()
        model.resetLevel(level.board, gaps: level.gaps)
        gameView.setRedoEnabled(false)
        scene.loadPreviousLevel {
            self.updatePreviousLevelNavigationOption()
            self.updateNextLevelNavigationOption()
        }
        
        updatePreviousLevelNavigationOption(true)
        updateNextLevelNavigationOption(true)
    }
    
    func rateSelected() {
        UIApplication.sharedApplication().openURL(NSURL(string:
            "https://itunes.apple.com/us/app/shifted-an-escape-puzzle/id1107839000?ls=1&mt=8")!)
    }
    
    func redoSelected() -> (() -> Void)! {
        gameState = .Over
        resetLevel()
        gameView.userInteractionEnabled = false
        
        return {[weak self] in
            self?.gameView.closeMenu()
            self?.gameView.userInteractionEnabled = true
        }
    }
    
    //MARK: GameProtocol
    func moveTile(from: (Int, Int), to: (Int, Int)) {
        scene.moveTile(from: from, to: to)
    }
    
    func pushTile(
        from: (Int, Int),
        to: (Int, Int),
        delay: Int,
        pusherDistance: Int,
        levelLost: Bool) {
        
        let state = gameState
        
        scene.moveTile(
            from: from,
            to: to,
            delay: delay,
            pusherDistance: pusherDistance) {
                switch state {
                case .Over:
                    break
                default:
                    if levelLost { self.resetLevel() }
                }
            }
        
        if levelLost { gameState = .Over }
    }
    
    func moveOut(
        from: (Int, Int),
        direction: Direction,
        distance: Int,
        levelCompleted: Bool) {
            
        if levelCompleted {
            gameState = .Completed
            levelWon()
            gameView.showLevelCompleted(completedLevels.count)
        }
        
        scene.moveOut(from: from, direction: direction, distance: distance)
    }
    
    func moveOutPush(
        from: (Int, Int),
        direction: Direction,
        pusherDistance: Int,
        delay: Int) {
            
        scene.moveOut(
            from: from,
            direction: direction,
            distance: pusherDistance,
            delay: delay)
    }
    
    func bombTick(source: (Int, Int), levelLost: Bool) {
        if levelLost { gameState = .Over }
        
        scene.bombTick(source) { if levelLost { self.resetLevel() } }
    }
    
    //MARK: - GameViewDelegate
    func slide(direction: Direction) {
        switch gameState {
        case .Active:
            model.slide(direction)
        case let .TutorialActive(solution: s, index: i):
            if direction != s[i] { return }
            scene.resetFocus(s[i]) {
                self.model.slide(direction)
                self.gameState = .Active
            }
        default:
            return
        }
    }
    
    func boardWidth() -> CGFloat {
        return level(.Current)!.boardData.boardWidth
    }
    
    func menuWillOpen() -> Bool {
        switch gameState {
        case .Active:
            gameState = .Paused
            return true
        default:
            return false
        }
    }
    
    func menuWillClose() {
        switch gameState {
        case .TransitionPaused:
            gameState = .TransitionActive
        default:
            gameState = .Active
        }
    }
    
    //MARK: Observer Callbacks
    func removeMusicNode() {
        scene.removeMusicNode()
    }
    
    //MARK: Private
    
    private func resetLevel() {
        guard let current = level(.Current) else { return }
        model.resetLevel(current.board, gaps: current.gaps)
        scene.restartLevel()
    }
    
    private func addAppStateObservers() {
        NSNotificationCenter.defaultCenter().addObserver(self,
            selector: #selector(GameController.removeMusicNode),
            name: UIApplicationWillResignActiveNotification, object: nil)
    }
    
    private func removeAppStateObservers() {
        NSNotificationCenter.defaultCenter().removeObserver(self)
    }
    
    private func updatePreviousLevelNavigationOption(onlyOnDisabled: Bool = false) {
        if onlyOnDisabled &&
            levelDataSource.levelWithIDExists(currentLevelID - 1) { return }
        gameView.updatePreviousLevelOption(
            !levelDataSource.levelWithIDExists(currentLevelID - 1))
    }
    
    private func updateNextLevelNavigationOption(onlyOnDisabled: Bool = false) {
        if onlyOnDisabled &&
            levelDataSource.levelWithIDExists(currentLevelID + 1) { return }
        gameView.updateNextLevelOption(
            !levelDataSource.levelWithIDExists(currentLevelID + 1))
    }
    
    private func updateLastLevelPlayed() {
        let defaults = NSUserDefaults.standardUserDefaults()
        defaults.setInteger(currentLevelID, forKey: "lastLevel")
    }
    
    private func setCurrentLevelToLastPlayed() {
        let defaults = NSUserDefaults.standardUserDefaults()
        currentLevelID = max(defaults.integerForKey("lastLevel"),1)
    }
    
    private func setCompletedLevels() {
        let defaults = NSUserDefaults.standardUserDefaults()
        guard let completed = defaults.stringForKey("completed") else { return }
        completed.componentsSeparatedByString(",").forEach({level in
            if let levelAsInt = Int(level) {
                completedLevels.insert(levelAsInt)
            }
        })
    }
    
    private func updateCompletedLevels() {
        let defaults = NSUserDefaults.standardUserDefaults()
        var completedString = ""
        completedLevels.forEach({level in
            completedString += String(level) + ","
        })
        
        defaults.setObject(completedString, forKey: "completed")
    }
    
    private func hasCompletedTutorialLevel() -> Bool {
        let defaults = NSUserDefaults.standardUserDefaults()
        return defaults.boolForKey("firstLevelCompleted")
    }
    
    private func markTutorialCompleted() {
        let defaults = NSUserDefaults.standardUserDefaults()
        defaults.setBool(true, forKey: "firstLevelCompleted")
    }
    
    private func updateInitialGameState() {
        if !hasCompletedTutorialLevel() {
            guard let solution =
                levelDataSource.solutionForLevel(currentLevelID) else { return }
            
            gameState = .TutorialInactive(solution: solution,index: 0)
        }
    }
    
    override func prefersStatusBarHidden() -> Bool {
        return true
    }
}
