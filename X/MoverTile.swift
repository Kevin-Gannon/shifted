//
//  MoverTile.swift
//  X
//
//  Created by Kevin Gannon on 8/11/15.
//  Copyright (c) 2015 Kevin Gannon. All rights reserved.
//

import UIKit
import SpriteKit

struct MoverTile: TileNodeProtocol {
    private(set) var node: SKSpriteNode
    private(set) var startingCoordinate: BoardCoordinate
    private(set) var direction: Direction
    
    private var tile: TileNode
    
    init(tile: TileNode, direction: Direction) {
        self.tile = tile
        self.startingCoordinate = tile.startingCoordinate
        self.direction = direction
        
        tile.node.zPosition = 4.0
        node = tile.node
    }
    
    func move(stop: Stop, completion: (() -> Void)!) {
        tile.moveTile(stop, completion: completion)
    }
    
    func resetToPosition(position: CGPoint) {
        tile.node.position = position
    }
    
    func moveOut(stop: Stop, completion: (() -> Void)!) {
        let color = Color.moverColor(direction)
        tile.moveOut(stop, color: color, completion: completion)
    }
}
