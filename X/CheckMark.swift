
//
//  CheckMarkView.swift
//  X
//
//  Created by Kevin Gannon on 3/5/16.
//  Copyright © 2016 Kevin Gannon. All rights reserved.
//

import UIKit

protocol CheckMarkDelegate : class {
    func loadNextLevel(checkMark: CheckMark)
}

class CheckMark: UIView {
    
    private var check: UIImageView
    private var nextArrow: UIImageView
    
    weak var delegate: CheckMarkDelegate?
    
    init() {
        check = UIImageView(image: UIImage(named: "check"))
        nextArrow = UIImageView(image: UIImage(named: "nextArrow"))
        
        let greenRing = UIImageView(image: UIImage(named: "greenRing"))
        
        super.init(frame: CGRectZero)
        
        clipsToBounds = true
        
        frame.size = greenRing.bounds.size
        backgroundColor = UIColor.clearColor()
        
        check.center = center
        
        nextArrow.center.y = center.y
        nextArrow.alpha = 0.0
        nextArrow.frame.origin.x = bounds.size.width
        
        let darkenedBackground = UIView(frame: CGRectZero)
        darkenedBackground.frame.size = frame.size
        darkenedBackground.backgroundColor = Color.backgroundColor()
        darkenedBackground.alpha = 0.4
        darkenedBackground.layer.cornerRadius = frame.size.width / 2.0
        
        addSubview(darkenedBackground)
        addSubview(greenRing)
        addSubview(nextArrow)
        addSubview(check)
    }
    
    func revealNextLevelOption(delay: Double = 0.0) {
        UIView.animateWithDuration(0.6, delay: delay, options: .CurveEaseOut, animations: {
            self.check.alpha = 0.0
        }, completion: nil)
        
        UIView.animateWithDuration(0.6, delay: delay + 0.2, options: [], animations: {
            self.nextArrow.frame.origin.x = self.bounds.size.width / 2.0 -
                (self.nextArrow.bounds.size.width / 2.0)
            }, completion: nil)
        
        UIView.animateWithDuration(0.6, delay: delay + 0.2, options: [], animations: {
            let width = self.check.bounds.size.width
            self.check.frame.origin.x = -width
            }, completion: nil)
        
        UIView.animateWithDuration(0.6, delay: delay + 0.6, options: .CurveEaseOut, animations: {
            self.nextArrow.alpha = 1.0
            }, completion: {done in self.addTapGesture()})
    }
    
    func tapped(sender: UITapGestureRecognizer) {
        removeGestureRecognizer(sender)
        UIView.animateWithDuration(0.18, animations: {
            self.transform = CGAffineTransformMakeScale(0.9, 0.9)
            }, completion: { done in
                UIView.animateWithDuration(0.16, animations: {
                    self.transform = CGAffineTransformMakeScale(1.0, 1.0)
                    }, completion: {finished in
                        self.delegate?.loadNextLevel(self)
                })
        })
    }
    
    private func addTapGesture() {
        let tapGesture = UITapGestureRecognizer(
            target: self,
            action: #selector(CheckMark.tapped(_:)))
        
        addGestureRecognizer(tapGesture)
    }

    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}
