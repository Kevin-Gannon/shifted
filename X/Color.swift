//
//  Color.swift
//  X
//
//  Created by Kevin Gannon on 9/6/15.
//  Copyright (c) 2015 Kevin Gannon. All rights reserved.
//

import UIKit

func hueify(color: UIColor, brightnessFactor: CGFloat = 0.7) -> UIColor? {
    var h: CGFloat = 0.0
    var s: CGFloat = 0.0
    var b: CGFloat = 0.0
    var a: CGFloat = 0.0
    
    if color.getHue(&h, saturation: &s, brightness: &b, alpha: &a) {
        return UIColor(hue: h, saturation: s, brightness: b * brightnessFactor, alpha: a)
    }
    
    return nil
}

class Color {    
    static func moverColor(direction: Direction) -> UIColor {
        switch direction {
        case .Up:
            return hueify(UIColor(red:0.847, green:0.376, blue:0.376, alpha:1), brightnessFactor: 1.3)!
        case .Down:
            return UIColor(red:0.529, green:0.831, blue:0.588, alpha:1)
        case .Right:
            return hueify(UIColor(red:0.917, green:0.647, blue:0.301, alpha:1), brightnessFactor: 1.3)!
        case .Left:
            return UIColor(red:0.498, green:0.745, blue:0.882, alpha:1)
        }
    }
    
    static func bombTextColor() -> UIColor {
        return UIColor.whiteColor()
    }
    
    static func transitionTextColor() -> UIColor {
        return UIColor(red:0.925, green:0.941, blue:0.945, alpha:1)
    }
    
    static func borderOutline() -> UIColor {
       return UIColor(red:0.964, green:0.941, blue:0.501, alpha:1)
    }
    
    static func borderColor() -> UIColor {
        return UIColor(red:0.498, green:0.549, blue:0.552, alpha:1)
    }
    
    static func borderColor2() -> UIColor {
        return UIColor(red:0.521, green:0.584, blue:0.588, alpha:1)
    }
    
    static func menuButtonFill() -> UIColor {
        return UIColor(red:0.498, green:0.549, blue:0.552, alpha:0.6)
    }
    
    static func levelCompletedText() -> UIColor {
        return UIColor(red:0.741, green:0.764, blue:0.78, alpha:1)
    }
    
    static func emptyTileColor() -> UIColor {
        return UIColor(red:0.301, green:0.301, blue:0.294, alpha:1)
    }
    
    static func levelCompletionModal() -> UIColor {
        return UIColor(red:0.301, green:0.301, blue:0.294, alpha:0.9)
    }
    
    static func bombColor() -> UIColor {
        return UIColor(red:0.521, green:0.564, blue:0.811, alpha:1)
    }
    
    static func menuButtonColor() -> UIColor {
        return hueify(emptyTileColor(), brightnessFactor: 1.3)!
    }
    
    static func backgroundColor() -> UIColor {
        return UIColor(red:0.176, green:0.176, blue:0.176, alpha:1)
    }
}
