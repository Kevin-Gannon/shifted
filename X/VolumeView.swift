//
//  VolumeView.swift
//  X
//
//  Created by Kevin Gannon on 9/27/15.
//  Copyright © 2015 Kevin Gannon. All rights reserved.
//

import UIKit

protocol ToggleView {
    func toggle()
}

class VolumeView: UIView, ToggleView {
    
    private let rightOuterRing: VolumeRing
    private let rightMiddleRing: VolumeRing
    private let rightInnerRing: VolumeRing
    
    private let leftOuterRing: VolumeRing
    private let leftMiddleRing: VolumeRing
    private let leftInnerRing: VolumeRing
    
    private let innerRingSpacing: CGFloat = 3.5
    private let middleRingSpacing: CGFloat = 3.5
    private let fromCenterOffset: CGFloat = 5.0
    
    private var toggleAction: (() -> Void)!
    
    override init(frame: CGRect) {
        rightOuterRing = VolumeRing(ringStyle: .Outer)
        rightOuterRing.transform = CGAffineTransformMakeScale(0.5, 0.5)
        
        rightMiddleRing = VolumeRing(ringStyle: .Middle)
        rightMiddleRing.transform = CGAffineTransformMakeScale(0.5, 0.5)
        
        rightInnerRing = VolumeRing(ringStyle: .Inner)
        rightInnerRing.transform = CGAffineTransformMakeScale(0.5, 0.5)
        
        leftOuterRing = VolumeRing(ringStyle: .Outer)
        leftOuterRing.transform = CGAffineTransformConcat(
            CGAffineTransformMakeScale(0.5, 0.5),
            CGAffineTransformMakeRotation(CGFloat(M_PI)))

        leftMiddleRing = VolumeRing(ringStyle: .Middle)
        leftMiddleRing.transform = CGAffineTransformConcat(
            CGAffineTransformMakeScale(0.5, 0.5),
            CGAffineTransformMakeRotation(CGFloat(M_PI)))
        
        leftInnerRing = VolumeRing(ringStyle: .Inner)
        leftInnerRing.transform = CGAffineTransformConcat(
            CGAffineTransformMakeScale(0.5, 0.5),
            CGAffineTransformMakeRotation(CGFloat(M_PI)))
    
        super.init(frame: frame)
        backgroundColor = UIColor.clearColor()
        
        rightInnerRing.center = self.center
        rightInnerRing.center.x += fromCenterOffset
        
        leftInnerRing.center = self.center
        leftInnerRing.center.x -= fromCenterOffset
        
        rightMiddleRing.center = rightInnerRing.center
        rightInnerRing.center.y += 0.5
        rightMiddleRing.center.x += innerRingSpacing
        
        leftMiddleRing.center = leftInnerRing.center
        leftMiddleRing.center.x -= innerRingSpacing
        
        rightOuterRing.center = rightMiddleRing.center
        rightOuterRing.center.x += middleRingSpacing
        
        leftOuterRing.center = leftMiddleRing.center
        leftOuterRing.center.x -= middleRingSpacing

        addSubview(rightInnerRing)
        addSubview(leftInnerRing)
        
        addSubview(rightMiddleRing)
        addSubview(leftMiddleRing)
        
        addSubview(rightOuterRing)
        addSubview(leftOuterRing)
        
        toggleAction = toggleOff
    }
    
    //MARK: Private
    private func toggleOff() {
        toggleAction = {[weak self] in self?.toggleOn()}
        
        rightOuterRing.setInactive()
        leftOuterRing.setInactive()
        
        let middleDelay = dispatch_time(DISPATCH_TIME_NOW,
                                        Int64(0.1 * Double(NSEC_PER_SEC)))
        
        dispatch_after(middleDelay, dispatch_get_main_queue()) {[weak self] in
            self?.rightMiddleRing.setInactive()
            self?.leftMiddleRing.setInactive()
        }
        
        let innerDelay = dispatch_time(DISPATCH_TIME_NOW,
                                       Int64(0.2 * Double(NSEC_PER_SEC)))
        
        dispatch_after(innerDelay, dispatch_get_main_queue()) {[weak self] in
            self?.rightInnerRing.setInactive()
            self?.leftInnerRing.setInactive()
        }
    }
    
    private func toggleOn() {
        toggleAction = {[weak self] in self?.toggleOff()}
        
        rightInnerRing.setActive()
        leftInnerRing.setActive()
        
        let middleDelay = dispatch_time(DISPATCH_TIME_NOW,
                                        Int64(0.1 * Double(NSEC_PER_SEC)))
        
        dispatch_after(middleDelay, dispatch_get_main_queue()) {[weak self] in
            self?.rightMiddleRing.setActive()
            self?.leftMiddleRing.setActive()
        }
        
        let innerDelay = dispatch_time(DISPATCH_TIME_NOW,
                                       Int64(0.2 * Double(NSEC_PER_SEC)))
        
        dispatch_after(innerDelay, dispatch_get_main_queue()) {[weak self] in
            self?.rightOuterRing.setActive()
            self?.leftOuterRing.setActive()
        }
    }
    
    //MARK: ToggleView
    func toggle() {
        toggleAction()
    }

    //MARK: Required
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}
