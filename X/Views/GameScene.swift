//
//  GameScene.swift
//  X
//
//  Created by Kevin Gannon on 7/5/15.
//  Copyright (c) 2015 Kevin Gannon. All rights reserved.
//

import SpriteKit
import UIKit

class GameScene: SKScene {
    
    private var backgroundNode: SKSpriteNode!
    private var pauseNode: SKSpriteNode!
    private var levels = Dictionary<LevelState,LevelNode>()
    
    private let musicNode: SKAudioNode = SKAudioNode(fileNamed: "loop")
    private let levelTextNode = SKLabelNode(fontNamed:
        "AvenirNextCondensed-MediumItalic")
    
    weak var sceneDelegate: SceneProtocol!
    
    override func didMoveToView(view: SKView) {
        super.didMoveToView(view)
        
        addChild(musicNode)
        
        setupGameBoard()
        setupLevelText()
        showLevelText()
    }
    
    func removeMusicNode() {
        musicNode.runAction(SKAction.stop())
    }
    
    func moveTile(
        from from: (Int, Int),
        to: (Int, Int),
        delay: Int = 0,
        pusherDistance: Int = 1,
        completion: (() -> Void)! = nil) {
        
        let fromKey = coordinateFromLocation(from)
        let toKey = coordinateFromLocation(to)
        currentLevel().moveTile(fromKey, toKey: toKey, delay: delay,
            pusherDistance: pusherDistance, completion: completion)
    }
    
    func moveOut(
        from from: (Int, Int),
        direction: Direction,
        distance: Int,
        delay: Int = 0,
        completion: (() -> Void)! = nil) {
            
        let fromKey = coordinateFromLocation(from)
        currentLevel().moveOut(fromKey, direction: direction,
            distance: distance, delay: delay, completion: completion)
    }
    
    func bombTick(from: (Int, Int), completion: (() -> Void)!) {
        let fromKey = coordinateFromLocation(from)
        currentLevel().bombTick(fromKey,completion: completion)
    }
    
    func restartLevel() {
        fadeOutBoard(currentLevel()) {
            self.currentLevel().resetTilesToStartingPositions(false)
            self.showLevelText()
        }
    }
    
    func playMusic() {
        musicNode.runAction(SKAction.play())
    }
    
    func pauseMusic() {
        musicNode.runAction(SKAction.pause())
    }
    
    func focusTilesWithDirection(
        direction: Direction,
        withBomb: Bool = false,
        withSwipeGesture: Bool = true,
        completion: (() -> Void)!) {
            
        currentLevel().setFocusToTilesWithDirection(
            direction, withBomb: withBomb)
        pauseNode.runAction(SKAction.fadeAlphaTo(0.85, duration: 0.25)) {
            completion?()
            
            if withSwipeGesture { self.showSwipeGesture(direction) }
        }
    }
    
    func resetFocus(direction: Direction, completion: (() -> Void)!) {
        pauseNode.runAction(SKAction.fadeOutWithDuration(0.0)) {
            self.currentLevel().resetFocusOfTilesWithDirection(
                direction, withBomb: true)
            completion?()
        }
        
        if let swipdeNode = childNodeWithName("swipeNode") {
            swipdeNode.removeAllActions()
            swipdeNode.runAction(SKAction.fadeOutWithDuration(0.0))
        }
    }
    
    func loadNextLevel(currentLevelFadedCompletion: (() -> Void)!) {
        guard let next = levels[.Next] else { return }
        
        if next.parent == nil { addChild(next) }
        
        levelTextNode.removeAllActions()
        
        addLevel(.Next)
        
        levels[.Previous] = currentLevel()
        levels[.Current] = next
        
        fadeOutBoard(levels[.Previous]!) {
            self.levels[.Previous]?.resetTilesToStartingPositions()
            self.levels[.Previous]?.removeFromParent()
            self.showLevelText()
            currentLevelFadedCompletion?()
        }
    }
    
    func loadPreviousLevel(currentLevelFadedCompletion: (() -> Void)!) {
        guard let previous = levels[.Previous] else { return }
        
        if previous.parent == nil { addChild(previous) }
        
        levelTextNode.removeAllActions()
        
        addLevel(.Previous)
        
        levels[.Next] = currentLevel()
        levels[.Current] = previous
        
        fadeOutBoard(levels[.Next]!) {
            self.levels[.Next]?.resetTilesToStartingPositions()
            self.levels[.Next]?.removeFromParent()
            self.showLevelText()
            currentLevelFadedCompletion?()
        }
    }
    
    //MARK: - Private
    
    private func fadeOutBoard(levelNode: LevelNode, completion: (() -> Void)!) {
        let fadeDuration = 0.2
        
        if levelTextNode.alpha != 0.0 {
            
            for child in levelNode.board.children {
                child.removeAllActions()
            }
            
            levelNode.border.removeAllActions()
            
            completion?()
            
            return
        }
        
        for child in levelNode.board.children {
            child.removeAllActions()
            child.runAction(SKAction.fadeOutWithDuration(fadeDuration))
        }
        
        levelNode.border.removeAllActions()
        levelNode.border.runAction(SKAction.fadeOutWithDuration(fadeDuration)) {
            completion?()
        }
    }
    
    private func setupLevelText() {
        levelTextNode.alpha = 0.0
        levelTextNode.verticalAlignmentMode = .Center
        levelTextNode.fontSize = 40.0
        levelTextNode.fontColor = Color.transitionTextColor()
        
        addChild(levelTextNode)
    }
    
    private func setupGameBoard() {
        addLevel(.Current)
        addLevel(.Previous)
        addLevel(.Next)
        
        addChild(currentLevel())
        backgroundColor = Color.backgroundColor()
        
        pauseNode = SKSpriteNode(texture: currentLevel().boardData.textureFromType(.Pause))
        pauseNode.position = view!.center
        pauseNode.alpha = 0.0
        pauseNode.zPosition = 99
        
        addChild(pauseNode)
    }
    
    private func showLevelText() {
        let textColor = sceneDelegate.isLevelCompleted(currentLevel().level.id)
            ? UIColor(red:0.529, green:0.831, blue:0.588, alpha:1) : UIColor.whiteColor()
        levelTextNode.text = "#\(currentLevel().level.id)"
        levelTextNode.fontColor = textColor
        levelTextNode.position = view!.center
        levelTextNode.alpha = 0.5
        levelTextNode.setScale(0.84)
        levelTextNode.zPosition = 100
        levelTextNode.removeAllActions()

        let fadeInAction = SKAction.fadeInWithDuration(0.3)
        let scaleAction = SKAction.scaleTo(1.0, duration: 1)
        let first = SKAction.group([fadeInAction,scaleAction])
        let fadeOutAction = SKAction.fadeOutWithDuration(0.15)
        
        levelTextNode.runAction(SKAction.sequence([first,fadeOutAction])) {
            self.fadeInLevel()
        }
    }
    
    private func fadeInLevel() {
        let innerTiles = currentLevel().insideNodes
        let outsideTiles = currentLevel().outsideNodes
        
        let fadeInAction = SKAction.fadeInWithDuration(0.4)
        fadeInAction.timingMode = .EaseInEaseOut
        
        for node in innerTiles {
            node.runAction(fadeInAction)
        }
        
        let ousideTilesDelay = SKAction.waitForDuration(0.1)
        let outsideAction = SKAction.sequence([ousideTilesDelay, fadeInAction])
        
        for node in outsideTiles {
            node.runAction(outsideAction)
        }
        
        let borderDelay = SKAction.waitForDuration(0.2)
        let borderAction = SKAction.sequence([borderDelay, fadeInAction])
        currentLevel().border.runAction(borderAction) {
            self.currentLevel().revealGaps() {
                self.sceneDelegate?.sceneLoaded()
            }
        }
    }
    
    private func addLevel(state: LevelState) {
        if let level = sceneDelegate.level(state) {
            addTiles(level,levelNode: addLevelNode(level, state: state))
        }
    }
    
    private func addTiles(level: Level, levelNode: LevelNode) {
        for (index,tile) in level.board.enumerate() {
            let row = index / level.dimension
            let col = index % level.dimension
            let coordinate = BoardCoordinate(row: row, col: col)
            levelNode.addTile(tile, coordinate: coordinate)
        }
    }
    
    private func currentLevel() -> LevelNode {
        return levels[.Current]!
    }
    
    private func addLevelNode(level: Level, state: LevelState) -> LevelNode {
        let levelNode = LevelNode(level: level)
        
        levelNode.border.alpha = 0.0
        
        levels[state] = levelNode
        
        return levelNode
    }
    
    private func coordinateFromLocation(location: (Int,Int)) -> BoardCoordinate {
        let (row,col) = location
        return BoardCoordinate(row: row, col: col)
    }
    
    private func showSwipeGesture(direction: Direction) {
        switch direction {
        case .Right:
            let swipeNode = SKSpriteNode(imageNamed: "swipe")
            swipeNode.position.x = view!.center.x - currentLevel().boardData.tileWidth
            swipeNode.position.y = view!.bounds.size.height / 6.0
            swipeNode.name = "swipeNode"
            addChild(swipeNode)
            
            let tileWidth = currentLevel().boardData.tileWidth

            let moveAction = SKAction.moveByX(tileWidth * 2, y: 0, duration: 1.0)
            let resetAction = SKAction.moveByX(-tileWidth * 2, y: 0, duration: 0.0)
            
            let waitFadeAction = SKAction.waitForDuration(0.5)
            let fadeOutAction = SKAction.fadeOutWithDuration(0.5)
            let fadeInAction = SKAction.fadeInWithDuration(0.0)
            let waitResetAction = SKAction.waitForDuration(0.4)
            
            let moveSequence = SKAction.sequence([moveAction,waitResetAction,resetAction])
            let fadeSequence = SKAction.sequence([waitFadeAction,fadeOutAction,
                waitResetAction,fadeInAction])
            
            swipeNode.runAction(SKAction.repeatActionForever(SKAction.group(
                [moveSequence,fadeSequence])))
        default:
            break
        }
    }
}
