//
//  Bomb.swift
//  X
//
//  Created by Kevin Gannon on 8/11/15.
//  Copyright (c) 2015 Kevin Gannon. All rights reserved.
//

import UIKit
import SpriteKit

struct BombNode: TileNodeProtocol {
    private var ticker: SKLabelNode
    private var tile: TileNode
    
    private(set) var node: SKSpriteNode
    private(set) var startingCoordinate: BoardCoordinate
    
    private let bombValue: Int
    
    let blastPath = NSBundle.mainBundle().pathForResource("MyParticle", ofType: "sks")!
    let blastPath2 = NSBundle.mainBundle().pathForResource("Blast", ofType: "sks")!
    
    init(tile: TileNode, bombValue: Int) {
        self.bombValue = bombValue
        self.startingCoordinate = tile.startingCoordinate
        self.tile = tile
        
        ticker = SKLabelNode(text: "\(bombValue)")
        ticker.fontColor = Color.bombTextColor()
        ticker.fontName = "AvenirNextCondensed-DemiBold"
        ticker.zPosition = 5.0
        ticker.verticalAlignmentMode = .Center
        ticker.fontSize = ceil(tile.node.size.height / 2.0)
        
        tile.node.color = Color.bombColor()
        tile.node.addChild(ticker)
        tile.node.zPosition = 4.0
        
        node = tile.node
    }
    
    func tick(delay: NSTimeInterval = 0.0, completion: (() -> Void)!) {
        node.runAction(SKAction.waitForDuration(delay), completion: {
            let value = Int(self.ticker.text!)!
            self.ticker.text = "\(value - 1)"
            
            if value - 1 == 0 {
                self.tile.explode(Color.bombColor()) {
                    completion?()
                }
            }
                
            else { completion?() }
        })
    }
    
    func currentBombValue() -> Int {
        return Int(self.ticker.text!)!
    }
    
    func move(stop: Stop, completion : (() -> Void)!) {
        tile.moveTile(stop) {
            self.tick(completion: completion)
        }
    }
    
    func resetToPosition(position: CGPoint) {
        ticker.text = "\(bombValue)"
        tile.node.position = position
    }
    
    func moveOut(stop: Stop,completion: (() -> Void)!) {
        tile.moveOut(stop, color: Color.bombColor(), completion: completion)
    }
}
