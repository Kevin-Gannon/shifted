//
//  Rate.swift
//  X
//
//  Created by Kevin Gannon on 1/3/16.
//  Copyright © 2016 Kevin Gannon. All rights reserved.
//

import UIKit

class Rate: UIView {
    
    init() {
        super.init(frame: CGRectMake(0, 0, 54, 51))
        
        backgroundColor = UIColor.clearColor()
    }

    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    override func drawRect(rect: CGRect) {
        let fillColor = UIColor.whiteColor()
        let bezierPath = UIBezierPath()
        bezierPath.moveToPoint(CGPointMake(51.6, 18))
        bezierPath.addLineToPoint(CGPointMake(34.8, 16.6))
        bezierPath.addLineToPoint(CGPointMake(28.3, 1.1))
        bezierPath.addCurveToPoint(CGPointMake(26.6, 0), controlPoint1: CGPointMake(28, 0.4), controlPoint2: CGPointMake(27.3, 0))
        bezierPath.addCurveToPoint(CGPointMake(24.9, 1.1), controlPoint1: CGPointMake(25.9, 0), controlPoint2: CGPointMake(25.2, 0.4))
        bezierPath.addLineToPoint(CGPointMake(18.4, 16.6))
        bezierPath.addLineToPoint(CGPointMake(1.6, 18))
        bezierPath.addCurveToPoint(CGPointMake(0, 19.3), controlPoint1: CGPointMake(0.9, 18.1), controlPoint2: CGPointMake(0.2, 18.6))
        bezierPath.addCurveToPoint(CGPointMake(0.6, 21.3), controlPoint1: CGPointMake(-0.2, 20), controlPoint2: CGPointMake(0, 20.8))
        bezierPath.addLineToPoint(CGPointMake(13.3, 32.3))
        bezierPath.addLineToPoint(CGPointMake(9.5, 48.7))
        bezierPath.addCurveToPoint(CGPointMake(10.2, 50.6), controlPoint1: CGPointMake(9.3, 49.4), controlPoint2: CGPointMake(9.6, 50.2))
        bezierPath.addCurveToPoint(CGPointMake(12.2, 50.7), controlPoint1: CGPointMake(10.8, 51), controlPoint2: CGPointMake(11.6, 51.1))
        bezierPath.addLineToPoint(CGPointMake(26.6, 42))
        bezierPath.addLineToPoint(CGPointMake(41, 50.7))
        bezierPath.addCurveToPoint(CGPointMake(42, 51), controlPoint1: CGPointMake(41.3, 50.9), controlPoint2: CGPointMake(41.6, 51))
        bezierPath.addCurveToPoint(CGPointMake(43.1, 50.7), controlPoint1: CGPointMake(42.4, 51), controlPoint2: CGPointMake(42.8, 50.9))
        bezierPath.addCurveToPoint(CGPointMake(43.8, 48.8), controlPoint1: CGPointMake(43.7, 50.3), controlPoint2: CGPointMake(44, 49.5))
        bezierPath.addLineToPoint(CGPointMake(40, 32.4))
        bezierPath.addLineToPoint(CGPointMake(52.7, 21.4))
        bezierPath.addCurveToPoint(CGPointMake(53.3, 19.4), controlPoint1: CGPointMake(53.3, 20.9), controlPoint2: CGPointMake(53.5, 20.1))
        bezierPath.addCurveToPoint(CGPointMake(51.6, 18), controlPoint1: CGPointMake(53, 18.6), controlPoint2: CGPointMake(52.3, 18.1))
        bezierPath.closePath()
        bezierPath.moveToPoint(CGPointMake(36.6, 30.2))
        bezierPath.addCurveToPoint(CGPointMake(36, 32), controlPoint1: CGPointMake(36.1, 30.7), controlPoint2: CGPointMake(35.9, 31.3))
        bezierPath.addLineToPoint(CGPointMake(39.1, 45.2))
        bezierPath.addLineToPoint(CGPointMake(27.5, 38.2))
        bezierPath.addCurveToPoint(CGPointMake(26.5, 37.9), controlPoint1: CGPointMake(27.2, 38), controlPoint2: CGPointMake(26.9, 37.9))
        bezierPath.addCurveToPoint(CGPointMake(25.5, 38.2), controlPoint1: CGPointMake(26.2, 37.9), controlPoint2: CGPointMake(25.8, 38))
        bezierPath.addLineToPoint(CGPointMake(13.9, 45.2))
        bezierPath.addLineToPoint(CGPointMake(17, 32))
        bezierPath.addCurveToPoint(CGPointMake(16.4, 30.2), controlPoint1: CGPointMake(17.2, 31.3), controlPoint2: CGPointMake(16.9, 30.6))
        bezierPath.addLineToPoint(CGPointMake(6.1, 21.3))
        bezierPath.addLineToPoint(CGPointMake(19.6, 20.2))
        bezierPath.addCurveToPoint(CGPointMake(21.1, 19.1), controlPoint1: CGPointMake(20.3, 20.1), controlPoint2: CGPointMake(20.9, 19.7))
        bezierPath.addLineToPoint(CGPointMake(26.4, 6.6))
        bezierPath.addLineToPoint(CGPointMake(31.7, 19.1))
        bezierPath.addCurveToPoint(CGPointMake(33.2, 20.2), controlPoint1: CGPointMake(32, 19.7), controlPoint2: CGPointMake(32.6, 20.2))
        bezierPath.addLineToPoint(CGPointMake(46.7, 21.3))
        bezierPath.addLineToPoint(CGPointMake(36.6, 30.2))
        bezierPath.closePath()
        bezierPath.miterLimit = 4;
        
        fillColor.setFill()
        bezierPath.fill()
    }
}
