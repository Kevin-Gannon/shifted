//
//  MenuButton.swift
//  X
//
//  Created by Kevin Gannon on 9/14/15.
//  Copyright (c) 2015 Kevin Gannon. All rights reserved.
//

import UIKit

class MenuButton: UIView {
    
    private let startScale: CGFloat
    private let animateDuration: Double = 0.48
    private let imageView: UIView
    
    var isEnabled: Bool = true
    
    init(buttonSize: CGFloat, startScale: CGFloat, view: UIView) {
        self.startScale = startScale
        self.imageView = view
        
        super.init(frame: CGRectMake(0, 0, buttonSize, buttonSize))
        
        imageView.alpha = 0.0
        imageView.center = self.center
        
        addSubview(imageView)
    
        backgroundColor = Color.menuButtonFill()
        layer.borderColor = Color.menuButtonFill().CGColor
        layer.borderWidth = 1.5
        layer.cornerRadius = 0.5 * bounds.size.width
        transform = CGAffineTransformMakeScale(startScale, startScale)
        
        userInteractionEnabled = false
    }

    func tapped(completion: (() -> Void)!) {
        if let toggleView = imageView as? ToggleView {
            toggleView.toggle()
        }
        
        UIView.animateWithDuration(0.18, animations: {
            self.transform = CGAffineTransformMakeScale(0.88, 0.88)
            }, completion: { done in
                if !done { return }
                UIView.animateWithDuration(0.16, animations: {
                    self.transform = CGAffineTransformMakeScale(1.0, 1.0)
                    }, completion: {finished in completion?()
                })
        })
    }
    
    func open(xOffset: CGFloat, yOffset: CGFloat, delay: Double, completion: (() -> Void)!) {
        UIView.animateWithDuration(
            animateDuration, delay: delay, usingSpringWithDamping: 0.65,
            initialSpringVelocity: 0.5, options: .CurveEaseOut, animations: {
                self.backgroundColor = Color.backgroundColor()
                self.layer.borderColor = Color.borderColor().CGColor
                self.transform = CGAffineTransformMakeScale(1.0, 1.0)
                self.frame = CGRectApplyAffineTransform(self.frame,
                    CGAffineTransformMakeTranslation(xOffset, yOffset))
                self.imageView.alpha = 1.0
            }, completion: {done in completion?()
        })
    }
    
    func close(xOffset: CGFloat, yOffset: CGFloat, delay: Double, completion: (() -> Void)!) {
        layer.removeAllAnimations()
        UIView.animateWithDuration(
            animateDuration, delay: delay, usingSpringWithDamping: 0.75,
            initialSpringVelocity: 0.5, options: .CurveEaseOut, animations: {
                self.layer.borderColor = Color.menuButtonFill().CGColor
                self.backgroundColor = Color.menuButtonFill()
                self.transform = CGAffineTransformMakeScale(self.startScale, self.startScale)
                self.frame = CGRectApplyAffineTransform(self.frame,
                    CGAffineTransformMakeTranslation(xOffset, yOffset))
                self.imageView.alpha = 0.0
            }, completion: {done in completion?()
        })
    }
    
    func isActive() -> Bool {
        return transform.a == 1.0 && isEnabled
    }

    required init(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}
