//
//  GameView.swift
//  X
//
//  Created by Kevin Gannon on 12/19/15.
//  Copyright © 2015 Kevin Gannon. All rights reserved.
//

import UIKit
import SpriteKit

protocol GameViewDelegate : MenuDelegate, LevelNaviationDelegate {
    func slide(direction: Direction)
    func menuWillOpen() -> Bool
    func menuWillClose()
    func boardWidth() -> CGFloat
}

enum LevelNavState {
    case WaitForMinTimeNotApproved
    case WaitForMinTimeApproved(disabled: Bool)
    case WaitingForApproval
    case NotWaiting
}

class GameView: UIView, UIGestureRecognizerDelegate, CheckMarkDelegate  {
    
    private var menu: Menu
    private var leftNavigationArrow: LevelNavigationButton
    private var rightNavigationArrow: LevelNavigationButton
    private var handleMenuTap: () -> Void = {}
    private var handleViewTap: () -> Void = {}
    private var swipeRecognizers = [UISwipeGestureRecognizer]()
    private var leftNavState = LevelNavState.NotWaiting
    private var rightNavState = LevelNavState.NotWaiting
    private var boardView: SKView
    private var blurView: UIVisualEffectView
    
    weak var delegate: GameViewDelegate? { didSet { menu.delegate = delegate }}
    
    override init(frame: CGRect) {
        menu = Menu(buttonSize: 45.0)
        
        leftNavigationArrow = LevelNavigationButton(direction: .Left)
        rightNavigationArrow = LevelNavigationButton(direction: .Right)
        
        boardView = SKView(frame: frame)
        boardView.ignoresSiblingOrder = true
        
        blurView = UIVisualEffectView()
        
        super.init(frame: frame)
        
        menu.center.x = center.x
        menu.frame.origin.y = bounds.size.height - 45.0
        
        blurView.frame = frame
        
        leftNavigationArrow.frame.origin.x = -(leftNavigationArrow.bounds.width *
            abs(leftNavigationArrow.transform.a))
        leftNavigationArrow.center.y = center.y
        
        rightNavigationArrow.frame.origin.x = bounds.width
        rightNavigationArrow.center.y = center.y
        
        leftNavigationArrow.addTarget(self, action: #selector(GameView.previousLevelSelected),
                                      forControlEvents: .TouchUpInside)
        rightNavigationArrow.addTarget(self, action: #selector(GameView.nextLevelSelected),
                                       forControlEvents: .TouchUpInside)
        
        let menuTapped = UITapGestureRecognizer(target: self,
                                                action: #selector(GameView.menuTapped))
        menu.addGestureRecognizer(menuTapped)
        
        let viewTapped = UITapGestureRecognizer(target: self,
                                                action: #selector(GameView.viewTapped))
        viewTapped.delegate = self
        addGestureRecognizer(viewTapped)
        
        handleMenuTap = {[weak self] in self?.openMenu() }
        
        addSubview(boardView)
        addSubview(blurView)
        addSubview(menu)
        addSubview(leftNavigationArrow)
        addSubview(rightNavigationArrow)
        
        addSwipeControls()
    }

    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    func presentScene(scene: SKScene) {
        boardView.presentScene(scene)
    }
    
    func loadNextLevel(checkMark: CheckMark) {
        nextLevelSelected()
        
        UIView.animateWithDuration(0.5, delay: 0.2, options: [], animations: {
            self.blurView.effect = nil
            }, completion: { done in
                checkMark.removeFromSuperview()
        })
        
        UIView.animateWithDuration(0.2, delay: 0.0, options: [], animations: {
            checkMark.alpha = 0.0
            }, completion: { done in
                checkMark.removeFromSuperview()
        })
    }
    
    func showLevelCompleted(totalLevelCompleted: Int, delay: Double = 0.45) {
        UIView.animateWithDuration(0.4, delay: delay, options: [], animations: {
            self.blurView.effect = UIBlurEffect(style: .Dark)}, completion: nil)
        
        let checkMark = CheckMark()
        checkMark.delegate = self
        checkMark.alpha = 0
        checkMark.center = center
        checkMark.center.y += checkMark.bounds.size.height / 2.0

        addSubview(checkMark)
        
        UIView.animateWithDuration(0.4, delay: delay + 0.3,
                                   options: [], animations: {
            checkMark.center.y = self.center.y
        },  completion: nil)
        
        UIView.animateWithDuration(0.5, delay: delay + 0.35,
                                   options: .CurveEaseInOut, animations: {
            checkMark.alpha = 1
            }, completion: { done in checkMark.revealNextLevelOption()
        })
    }
    
    func gestureRecognizer(gestureRecognizer: UIGestureRecognizer,
        shouldReceiveTouch touch: UITouch) -> Bool {
        
        let location = touch.locationInView(self)
        
        if CGRectContainsPoint(leftNavigationArrow.frame, location) {
            return false
        }
        
        if CGRectContainsPoint(rightNavigationArrow.frame, location) {
            return false
        }
        
        return true
    }
    
    func updatePreviousLevelOption(disabled: Bool) {
        if disabled {
            leftNavigationArrow.disabled = disabled
            return
        }
        
        switch leftNavState {
        case .WaitingForApproval:
            leftNavigationArrow.disabled = disabled
        case .WaitForMinTimeNotApproved:
            leftNavState = .WaitForMinTimeApproved(disabled: disabled)
        default:
            return
        }
    }
    
    func updateNextLevelOption(disabled: Bool) {
        if disabled {
            rightNavigationArrow.disabled = disabled
            return
        }
        
        switch rightNavState {
        case .WaitForMinTimeNotApproved:
            rightNavState = .WaitForMinTimeApproved(disabled: disabled)
        default:
            rightNavigationArrow.disabled = disabled
        }
    }
    
    func setRedoEnabled(enabled: Bool) {
        menu.setRedoEnabled(enabled)
    }
    
    func viewTapped() {
        handleViewTap()
    }
    
    func menuTapped() {
        handleMenuTap()
    }
    
    func upSwipe() {
        delegate?.slide(.Up)
    }
    
    func downSwipe() {
        delegate?.slide(.Down)
    }
    
    func leftSwipe() {
        delegate?.slide(.Left)
    }
    
    func rightSwipe() {
        delegate?.slide(.Right)
    }
    
    func openMenu() {
        if delegate?.menuWillOpen() ?? true {
            menu.open()
            showLevelNavigationOptions()
            removeSwipeRecognizers()
            handleMenuTap = { [weak self] in self?.closeMenu() }
            handleViewTap = { [weak self] in self?.closeMenu() }
        }
    }
    
    func closeMenu() {
        delegate?.menuWillClose()
        addSwipeControls()
        menu.close()
        hideLevelNavigationOptions()
        handleMenuTap = { [weak self] in self?.openMenu() }
        handleViewTap = {}
    }
    
    func previousLevelSelected() {
        delayTouchOnNavigation()
        delegate?.previousLevelSelected()
    }
    
    func nextLevelSelected() {
        delayTouchOnNavigation()
        delegate?.nextLevelSelected()
    }
    
    //MARK: - Private 
    private func addSwipeControls() {
        let upSwipe = UISwipeGestureRecognizer(
            target: self, action: #selector(GameView.upSwipe))
        upSwipe.direction = .Up
        addGestureRecognizer(upSwipe)
        
        let downSwipe = UISwipeGestureRecognizer(
            target: self, action: #selector(GameView.downSwipe))
        downSwipe.direction = .Down
        addGestureRecognizer(downSwipe)
        
        let leftSwipe = UISwipeGestureRecognizer(
            target: self, action: #selector(GameView.leftSwipe))
        leftSwipe.direction = .Left
        addGestureRecognizer(leftSwipe)
        
        let rightSwipe = UISwipeGestureRecognizer(
            target: self, action: #selector(GameView.rightSwipe))
        rightSwipe.direction = .Right
        addGestureRecognizer(rightSwipe)
        
        swipeRecognizers = [upSwipe,downSwipe,leftSwipe,rightSwipe]
    }
    
    private func showLevelNavigationOptions() {
        guard let boardWidth = delegate?.boardWidth() else { return }
        
        let leftPosition = (((bounds.width - boardWidth) / 2.0) / 2.0) - 5.0
        let rightPosition = bounds.width - leftPosition
        
        UIView.animateWithDuration(0.25, delay: 0.0, options: .CurveEaseOut,
            animations: {
                self.leftNavigationArrow.center.x = leftPosition
                self.rightNavigationArrow.center.x = rightPosition
            }, completion: nil)
    }
    
    private func hideLevelNavigationOptions() {
        let leftPositon = -(self.leftNavigationArrow.bounds.width *
            abs(self.leftNavigationArrow.transform.a))
        
        UIView.animateWithDuration(0.4, delay: 0.0, options: .CurveEaseOut,
            animations: {
                self.leftNavigationArrow.frame.origin.x = leftPositon
                self.rightNavigationArrow.frame.origin.x = self.bounds.width
            }, completion: nil)
    }
    
    private func removeSwipeRecognizers() {
        for swipe in swipeRecognizers {
            removeGestureRecognizer(swipe)
        }
    }
    
    private func delayTouchOnNavigation() {
        leftNavigationArrow.userInteractionEnabled = false
        rightNavigationArrow.userInteractionEnabled = false
        
        leftNavState = .WaitForMinTimeNotApproved
        rightNavState = .WaitForMinTimeNotApproved
        
        let delayTime = dispatch_time(DISPATCH_TIME_NOW,
            Int64(0.1 * Double(NSEC_PER_SEC)))
        
        dispatch_after(delayTime, dispatch_get_main_queue()) {
            switch self.leftNavState {
            case let .WaitForMinTimeApproved(disabled: d) :
                self.leftNavigationArrow.disabled = d
            default:
                self.leftNavState = .WaitingForApproval
            }
            
            switch self.rightNavState {
            case let .WaitForMinTimeApproved(disabled: d) :
                self.rightNavigationArrow.disabled = d
            default:
                self.rightNavState = .WaitingForApproval
            }
        }
    }
}
