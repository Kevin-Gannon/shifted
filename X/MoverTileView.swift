//
//  MoverTileView.swift
//  X
//
//  Created by Kevin Gannon on 12/11/15.
//  Copyright © 2015 Kevin Gannon. All rights reserved.
//

import UIKit

class MoverTileView: TileView {
    
    init(color: UIColor, length: CGFloat, radius: CGFloat, direction: Direction) {
        super.init(color: color, length: length, radius: radius)
        
        addArrow(direction)
    }
    
    private func addArrow(direction: Direction) {
        let arrow = ArrowShape()
        let scale = (self.bounds.size.height / 2.0) / arrow.bounds.size.height
        
        arrow.transform = CGAffineTransformMakeScale(scale, scale)
        
        switch direction {
        case .Down:
            arrow.transform = CGAffineTransformConcat(arrow.transform, CGAffineTransformMakeRotation(CGFloat(M_PI / 2.0)))
        case .Up:
            arrow.transform = CGAffineTransformConcat(arrow.transform,
                CGAffineTransformMakeRotation(-CGFloat(M_PI / 2.0)))
        case .Left:
            arrow.transform = CGAffineTransformConcat(arrow.transform, CGAffineTransformMakeRotation(CGFloat(M_PI)))
        case .Right:
            break
        }
        
        arrow.center = self.center
        
        addSubview(arrow)
    }

    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}
