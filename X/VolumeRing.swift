//
//  OuterVolumeRing.swift
//  X
//
//  Created by Kevin Gannon on 9/27/15.
//  Copyright © 2015 Kevin Gannon. All rights reserved.
//

import UIKit

enum VolumeRingStyle {
    case Inner
    case Middle
    case Outer
}

class VolumeRing: UIView {
    
    private var currentColor: UIColor
    private var ringPath = UIBezierPath()
    
    private let activeColor = UIColor.whiteColor()
    private let inactiveColor = UIColor.lightGrayColor()
    
    private var outerRingPath: UIBezierPath {
        let bezierPath = UIBezierPath()
        
        bezierPath.moveToPoint(CGPointMake(2.8, 0.1))
        bezierPath.addLineToPoint(CGPointMake(0, 2.9))
        bezierPath.addCurveToPoint(CGPointMake(0, 40.2), controlPoint1: CGPointMake(10.4, 13.2), controlPoint2: CGPointMake(10.4, 29.9))
        bezierPath.addLineToPoint(CGPointMake(2.8, 43))
        bezierPath.addCurveToPoint(CGPointMake(3.1, 43), controlPoint1: CGPointMake(2.9, 43.1), controlPoint2: CGPointMake(3, 43.1))
        bezierPath.addCurveToPoint(CGPointMake(3.1, 0.1), controlPoint1: CGPointMake(14.9, 31.1), controlPoint2: CGPointMake(14.9, 12))
        bezierPath.addCurveToPoint(CGPointMake(2.8, 0.1), controlPoint1: CGPointMake(3, 0), controlPoint2: CGPointMake(2.9, 0))
        bezierPath.closePath()
        bezierPath.miterLimit = 4
        
        return bezierPath
    }
    
    private var innerRingPath: UIBezierPath {
        let bezierPath = UIBezierPath()
        
        bezierPath.moveToPoint(CGPointMake(2, 0.5))
        bezierPath.addCurveToPoint(CGPointMake(0.2, 2.3), controlPoint1: CGPointMake(1.5, 1), controlPoint2: CGPointMake(0.9, 1.6))
        bezierPath.addCurveToPoint(CGPointMake(0.2, 19.7), controlPoint1: CGPointMake(5, 7.1), controlPoint2: CGPointMake(5, 14.9))
        bezierPath.addLineToPoint(CGPointMake(2, 21.5))
        bezierPath.addCurveToPoint(CGPointMake(4.1, 21.4), controlPoint1: CGPointMake(2.6, 22.1), controlPoint2: CGPointMake(3.6, 22))
        bezierPath.addCurveToPoint(CGPointMake(4.1, 0.7), controlPoint1: CGPointMake(9.1, 15.4), controlPoint2: CGPointMake(9.1, 6.7))
        bezierPath.addCurveToPoint(CGPointMake(2, 0.5), controlPoint1: CGPointMake(3.6, 0), controlPoint2: CGPointMake(2.6, 0))
        bezierPath.closePath()
        bezierPath.miterLimit = 4
        
        return bezierPath
    }
    
    private var middleRingPath: UIBezierPath {
        let bezierPath = UIBezierPath()
        
        bezierPath.moveToPoint(CGPointMake(2.3, 0.3))
        bezierPath.addLineToPoint(CGPointMake(-0.1, 2.7))
        bezierPath.addCurveToPoint(CGPointMake(-0.1, 30.4), controlPoint1: CGPointMake(7.6, 10.3), controlPoint2: CGPointMake(7.6, 22.8))
        bezierPath.addLineToPoint(CGPointMake(2.3, 32.8))
        bezierPath.addCurveToPoint(CGPointMake(3.5, 32.8), controlPoint1: CGPointMake(2.6, 33.1), controlPoint2: CGPointMake(3.2, 33.1))
        bezierPath.addCurveToPoint(CGPointMake(3.5, 0.3), controlPoint1: CGPointMake(12.1, 23.7), controlPoint2: CGPointMake(12.1, 9.5))
        bezierPath.addCurveToPoint(CGPointMake(2.3, 0.3), controlPoint1: CGPointMake(3.2, -0.1), controlPoint2: CGPointMake(2.7, -0.1))
        bezierPath.closePath()
        bezierPath.miterLimit = 4
        
        return bezierPath
    }
    
    init(ringStyle: VolumeRingStyle) {
        self.currentColor = activeColor
        
        super.init(frame: CGRectZero)
        
        backgroundColor = UIColor.clearColor()
        
        switch ringStyle {
        case .Outer:
            ringPath = outerRingPath
            frame = CGRectMake(0, 0, 12.0, 43.0)
        case .Middle:
            ringPath = middleRingPath
            frame = CGRectMake(0, 0, 10.0, 33.0)
        case .Inner:
            ringPath = innerRingPath
            frame = CGRectMake(0, 0, 8.0, 23.0)
        }
    }

    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    func setActive() {
        currentColor = activeColor
        
        setNeedsDisplay()
    }
    
    func setInactive() {
        currentColor = inactiveColor
        
        setNeedsDisplay()
    }
    
    override func drawRect(rect: CGRect) {
        let fillColor = currentColor
        
        fillColor.setFill()
        ringPath.fill()
    }
}
