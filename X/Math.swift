//
//  Math.swift
//  X
//
//  Created by Kevin Gannon on 7/29/15.
//  Copyright (c) 2015 Kevin Gannon. All rights reserved.
//

import UIKit

func roundOff(value: CGFloat) -> CGFloat {
    let rounded = abs(floor(value) - value) < abs(ceil(value) - value)
        ? floor(value)
        : ceil(value)
    
    return rounded
}

func random(min min: CGFloat, max: CGFloat) -> CGFloat {
    return CGFloat(Float(arc4random()) / 0xFFFFFFFF) * (max - min) + min
}