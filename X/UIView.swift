//
//  UIView.swift
//  X
//
//  Created by Kevin Gannon on 8/29/15.
//  Copyright (c) 2015 Kevin Gannon. All rights reserved.
//

import UIKit

extension UIView {
    func takeSnapshot() -> UIImage {
        UIGraphicsBeginImageContextWithOptions(bounds.size, false, 0)
        layer.renderInContext(UIGraphicsGetCurrentContext()!)
        let image = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        return image
    }
}
