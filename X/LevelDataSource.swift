//
//  LevelTextures.swift
//  X
//
//  Created by Kevin Gannon on 12/11/15.
//  Copyright © 2015 Kevin Gannon. All rights reserved.
//

import UIKit
import SpriteKit

struct LevelDataSource {
    private var levelCache = Dictionary<Int,Level>()
    private var solutions = Dictionary<Int,[Direction]>()
    
    private let boardData: GameBoardData
    private let maxLevel = 30
    
    init(viewWidth: CGFloat, dimension: Int) {
        let padding: CGFloat = 2.0
        let boardMaxSize: CGFloat = UIScreen.mainScreen().scale == 3.0 ? 285 : 270
        var boardWidth = min(viewWidth - 90, boardMaxSize)
        let p = CGFloat(dimension-1) * padding
        let width = roundOff((boardWidth - p) / CGFloat(dimension))
        boardWidth = width * CGFloat(dimension) + p
        let strokeWidth: CGFloat = 7.0
        
        var textures = Dictionary<TextureType, SKTexture>()
        
        //Add the border texture
        let borderLength = boardWidth + strokeWidth * 2
        let borderView  = BorderView(length: borderLength, radius: 10.5,
                                     boardWidth: boardWidth, boardRadius: 3.0)
        
        textures[.Border] = SKTexture(image: borderView.takeSnapshot())
        
        //Add the Pause texture
        let pauseView = UIView(frame: CGRectMake(0, 0, boardWidth + 0.5, boardWidth + 0.5))
        pauseView.layer.cornerRadius = 3.0
        pauseView.backgroundColor = Color.backgroundColor()
        textures[.Pause] = SKTexture(image: pauseView.takeSnapshot())
        
        //Add the tile textures
        let moveDirections: Array<Direction> = [.Up,.Down,.Left,.Right]
        
        for direction in moveDirections {
            let tileView = MoverTileView(color: Color.moverColor(direction),
                length: width, radius: 3.0, direction: direction)
            let texture = SKTexture(image: tileView.takeSnapshot())
            
            textures[TextureType(rawValue: direction.rawValue)!] = texture
        }
        
        textures[.Bomb] = SKTexture(image: TileView(color: Color.bombColor(),
            length: width, radius: 3.0).takeSnapshot())
        
        let emptyTile = UIView(frame: CGRectMake(0, 0, width, width))
        emptyTile.backgroundColor = Color.emptyTileColor()
        emptyTile.layer.cornerRadius = 3.0
        
        let gapTexture = UIView(frame: CGRectMake(0, 0, width + 1, width + 1))
        gapTexture.backgroundColor = Color.backgroundColor()
        
        textures[.Gap] = SKTexture(image: gapTexture.takeSnapshot())
        textures[.Empty] = SKTexture(image: emptyTile.takeSnapshot())
        
        boardData = GameBoardData(boardWidth: boardWidth, tileWidth: width,
            padding: padding, textures: textures)
        
        addSolutions()
    }
    
    func levelWithIDExists(id: Int) -> Bool {
        return levelCache[id] != nil
    }
    
    func solutionForLevel(levelID: Int) -> [Direction]? {
        return solutions[levelID]
    }
    
    mutating func getLevel(levelID levelID: Int) -> Level? {
        let id = levelID > maxLevel ? 1 : levelID
        let path = NSBundle.mainBundle().pathForResource("Levels", ofType: "plist")
        let dict = NSDictionary(contentsOfFile: path!)
        
        for i in id-1...id+1 {
            
            if levelCache[i] != nil { continue }
            
            if let levelData = dict?.valueForKey("\(i)") as? Dictionary<String,AnyObject> {
                
                let dimension = levelData["Dimension"] as! Int
                
                var board = [Tile](count: dimension * dimension, repeatedValue: Empty())
                var gaps = [Gap]()
                
                for properties in levelData["Board"] as! Array<Dictionary<String,AnyObject>> {
                    let type = TileType(rawValue: properties["Type"]! as! String)!
                    let boardIndex = properties["Index"] as! Int
                    
                    switch type {
                    case .Mover:
                        let direction = Direction(rawValue: properties["Direction"]! as! String)!
                        board[boardIndex] = Mover(direction: direction)
                        
                    case .Bomb:
                        let value = properties["Value"] as! Int
                        board[boardIndex] = Bomb(value: value)
                        
                    case .Sitter:
                        board[boardIndex] = Sitter()
                        
                    default:
                        break
                    }
                }
                
                for properties in levelData["Gaps"] as! Array<Dictionary<String,AnyObject>> {
                    let gapDirection = Direction(rawValue: properties["Direction"] as! String)!
                    let gapIndex = properties["Index"] as! Int
                    
                    gaps.append(Gap(index: gapIndex, direction: gapDirection))
                }

                levelCache[i] = Level(id: i, dimension: dimension,
                    gaps: gaps, board: board, boardData: boardData)
            }
        }
        
        return levelCache[id]
    }
    
    private mutating func addSolutions() {
        solutions[1] = [.Right, .Up]
    }
}
