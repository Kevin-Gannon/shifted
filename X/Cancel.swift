//
//  Block.swift
//  X
//
//  Created by Kevin Gannon on 12/31/15.
//  Copyright © 2015 Kevin Gannon. All rights reserved.
//

import UIKit

class Cancel: UIView {
    
    init() {
        super.init(frame: CGRectMake(0, 0, 66, 66))
        
        backgroundColor = UIColor.clearColor()
    }

    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    override func drawRect(rect: CGRect) {
        let fillColor = Color.moverColor(.Up)
        let bezierPath = UIBezierPath()
        bezierPath.moveToPoint(CGPointMake(32.5, 61.75))
        bezierPath.addCurveToPoint(CGPointMake(13.03, 54.27), controlPoint1: CGPointMake(25.02, 61.75), controlPoint2: CGPointMake(18.21, 58.9))
        bezierPath.addLineToPoint(CGPointMake(54.27, 13.03))
        bezierPath.addCurveToPoint(CGPointMake(61.75, 32.5), controlPoint1: CGPointMake(58.9, 18.21), controlPoint2: CGPointMake(61.75, 25.02))
        bezierPath.addCurveToPoint(CGPointMake(32.5, 61.75), controlPoint1: CGPointMake(61.75, 48.63), controlPoint2: CGPointMake(48.63, 61.75))
        bezierPath.closePath()
        bezierPath.moveToPoint(CGPointMake(3.25, 32.5))
        bezierPath.addCurveToPoint(CGPointMake(32.5, 3.25), controlPoint1: CGPointMake(3.25, 16.37), controlPoint2: CGPointMake(16.37, 3.25))
        bezierPath.addCurveToPoint(CGPointMake(51.97, 10.73), controlPoint1: CGPointMake(39.98, 3.25), controlPoint2: CGPointMake(46.79, 6.1))
        bezierPath.addLineToPoint(CGPointMake(10.73, 51.97))
        bezierPath.addCurveToPoint(CGPointMake(3.25, 32.5), controlPoint1: CGPointMake(6.1, 46.79), controlPoint2: CGPointMake(3.25, 39.98))
        bezierPath.closePath()
        bezierPath.moveToPoint(CGPointMake(32.5, 0))
        bezierPath.addCurveToPoint(CGPointMake(0, 32.5), controlPoint1: CGPointMake(14.55, 0), controlPoint2: CGPointMake(0, 14.55))
        bezierPath.addCurveToPoint(CGPointMake(32.5, 65), controlPoint1: CGPointMake(0, 50.45), controlPoint2: CGPointMake(14.55, 65))
        bezierPath.addCurveToPoint(CGPointMake(65, 32.5), controlPoint1: CGPointMake(50.45, 65), controlPoint2: CGPointMake(65, 50.45))
        bezierPath.addCurveToPoint(CGPointMake(32.5, 0), controlPoint1: CGPointMake(65, 14.55), controlPoint2: CGPointMake(50.45, 0))
        bezierPath.closePath()
        
        fillColor.setFill()
        bezierPath.fill()
    }
}
