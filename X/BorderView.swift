//
//  BorderView.swift
//  X
//
//  Created by Kevin Gannon on 4/11/16.
//  Copyright © 2016 Kevin Gannon. All rights reserved.
//

import UIKit

class BorderView: UIView {
    private let dropShadowHeight: CGFloat = 6.0
    
    init(length: CGFloat, radius: CGFloat,
         boardWidth: CGFloat, boardRadius: CGFloat) {
        
        super.init(frame: CGRectMake(0, 0, length, length))
        
        layer.cornerRadius = radius
        backgroundColor = Color.borderOutline()
        
        addTopLayer(radius)
        addPlayableLayer(boardWidth, radius: boardRadius)
    }
    
    private func addTopLayer(radius: CGFloat) {
        var frame = self.bounds
        frame.size.height -= 5.0
        frame.size.width -= 5.0
        
        let topLayer = UIView(frame: frame)
        topLayer.backgroundColor = hueify(Color.emptyTileColor())
        topLayer.layer.cornerRadius = radius - 2.0
        
        topLayer.center = center
        
        addSubview(topLayer)
    }
    
    private func addPlayableLayer(length: CGFloat, radius: CGFloat) {
        let frame = CGRectMake(0, 0, length, length)

        let topLayer = UIView(frame: frame)
        topLayer.backgroundColor = Color.backgroundColor()
        topLayer.layer.cornerRadius = radius
        
        topLayer.center = center
        
        addSubview(topLayer)
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}
