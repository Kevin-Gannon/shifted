//
//  TileShapeNode.swift
//  X
//
//  Created by Kevin Gannon on 12/12/15.
//  Copyright © 2015 Kevin Gannon. All rights reserved.
//

import UIKit

class ArrowShape: UIView {
    
    init() {
        super.init(frame: CGRectMake(0, 0, 40, 59.0))
        
        backgroundColor = UIColor.clearColor()
    }

    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func drawRect(rect: CGRect) {
        let fillColor = UIColor.whiteColor()
        let bezierPath = UIBezierPath()
        
        bezierPath.moveToPoint(CGPointMake(37.4, 26.21))
        bezierPath.addLineToPoint(CGPointMake(7.22, 1.27))
        bezierPath.addCurveToPoint(CGPointMake(3.18, 0.61), controlPoint1: CGPointMake(6.16, 0.4), controlPoint2: CGPointMake(4.57, 0.14))
        bezierPath.addCurveToPoint(CGPointMake(0.9, 3.44), controlPoint1: CGPointMake(1.8, 1.08), controlPoint2: CGPointMake(0.9, 2.2))
        bezierPath.addLineToPoint(CGPointMake(16.73, 28.38))
        bezierPath.addLineToPoint(CGPointMake(0.9, 53.32))
        bezierPath.addCurveToPoint(CGPointMake(3.18, 56.14), controlPoint1: CGPointMake(0.9, 54.56), controlPoint2: CGPointMake(1.8, 55.67))
        bezierPath.addCurveToPoint(CGPointMake(4.6, 56.38), controlPoint1: CGPointMake(3.64, 56.3), controlPoint2: CGPointMake(4.13, 56.38))
        bezierPath.addCurveToPoint(CGPointMake(7.22, 55.48), controlPoint1: CGPointMake(5.57, 56.38), controlPoint2: CGPointMake(6.51, 56.07))
        bezierPath.addLineToPoint(CGPointMake(37.4, 30.54))
        bezierPath.addCurveToPoint(CGPointMake(38.49, 28.38), controlPoint1: CGPointMake(38.09, 29.97), controlPoint2: CGPointMake(38.49, 29.18))
        bezierPath.addCurveToPoint(CGPointMake(37.4, 26.21), controlPoint1: CGPointMake(38.49, 27.57), controlPoint2: CGPointMake(38.09, 26.78))
        bezierPath.closePath()
        
        fillColor.setFill()
        bezierPath.fill()
    }
}
