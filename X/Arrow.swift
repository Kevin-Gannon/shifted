//
//  Arrow.swift
//  X
//
//  Created by Kevin Gannon on 9/28/15.
//  Copyright © 2015 Kevin Gannon. All rights reserved.
//

import UIKit

class Arrow: UIView {
    
    private let fill: UIColor

    init(fillColor: UIColor = UIColor.whiteColor()) {
        
        self.fill = fillColor
        super.init(frame: CGRectMake(0, 0, 25.0, 43.0))
        
        backgroundColor = UIColor.clearColor()
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func drawRect(rect: CGRect) {
        let bezierPath = UIBezierPath()
        
        bezierPath.moveToPoint(CGPointMake(24.3, 20.7))
        bezierPath.addLineToPoint(CGPointMake(4.3, 0.7))
        bezierPath.addCurveToPoint(CGPointMake(2.9, 0.7), controlPoint1: CGPointMake(3.9, 0.3),
            controlPoint2: CGPointMake(3.3, 0.3))
        bezierPath.addLineToPoint(CGPointMake(1, 2.6))
        bezierPath.addCurveToPoint(CGPointMake(0.7, 3.3), controlPoint1: CGPointMake(0.8, 2.8),
            controlPoint2: CGPointMake(0.7, 3.1))
        bezierPath.addCurveToPoint(CGPointMake(1, 4), controlPoint1: CGPointMake(0.7, 3.6),
            controlPoint2: CGPointMake(0.8, 3.8))
        bezierPath.addLineToPoint(CGPointMake(18.4, 21.4))
        bezierPath.addLineToPoint(CGPointMake(1, 38.8))
        bezierPath.addCurveToPoint(CGPointMake(0.7, 39.5), controlPoint1: CGPointMake(0.8, 39),
            controlPoint2: CGPointMake(0.7, 39.3))
        bezierPath.addCurveToPoint(CGPointMake(1, 40.2), controlPoint1: CGPointMake(0.7, 39.8),
            controlPoint2: CGPointMake(0.8, 40))
        bezierPath.addLineToPoint(CGPointMake(2.9, 42.1))
        bezierPath.addCurveToPoint(CGPointMake(3.6, 42.4), controlPoint1: CGPointMake(3.1, 42.3),
            controlPoint2: CGPointMake(3.4, 42.4))
        bezierPath.addCurveToPoint(CGPointMake(4.3, 42.1), controlPoint1: CGPointMake(3.9, 42.4),
            controlPoint2: CGPointMake(4.1, 42.3))
        bezierPath.addLineToPoint(CGPointMake(24.3, 22.1))
        bezierPath.addCurveToPoint(CGPointMake(24.3, 20.7), controlPoint1: CGPointMake(24.7, 21.8),
            controlPoint2: CGPointMake(24.7, 21.1))
        bezierPath.closePath()
        bezierPath.miterLimit = 4;
        
        fill.setFill()
        bezierPath.fill()
    }
}
