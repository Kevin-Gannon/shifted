//
//  TileNode.swift
//  X
//
//  Created by Kevin Gannon on 8/11/15.
//  Copyright (c) 2015 Kevin Gannon. All rights reserved.
//

import UIKit
import SpriteKit

struct Stop: Equatable {
    var waitDuration: Double
    var location: CGPoint
    var duration: Double
    var timingMode: SKActionTimingMode
    
    init(wait: Double = 0.0, location: CGPoint, duration: Double, timingMode: SKActionTimingMode = .Linear) {
        self.waitDuration = wait
        self.location = location
        self.duration = duration
        self.timingMode = timingMode
    }
}

func ==(lhs: Stop, rhs: Stop) -> Bool {
    return lhs.waitDuration == rhs.waitDuration &&
    lhs.location == rhs.location &&
    lhs.duration == rhs.duration
}

protocol TileNodeProtocol {
    var node: SKSpriteNode { get }
    var startingCoordinate: BoardCoordinate { get }
    
    func resetToPosition(position: CGPoint)
    func move(stop: Stop, completion: (() -> Void)!)
    func moveOut(stop: Stop, completion: (() -> Void)!)
}

struct TileNode {
    var node: SKSpriteNode
    var startingCoordinate: BoardCoordinate
    
    let blastPath = NSBundle.mainBundle().pathForResource("MyParticle", ofType: "sks")!
    let blastPath2 = NSBundle.mainBundle().pathForResource("Blast", ofType: "sks")!
    
    init(startingCoordinate: BoardCoordinate, texture: SKTexture) {
        self.startingCoordinate = startingCoordinate
        node = SKSpriteNode(texture: texture)
    }
    
    func moveTile(stop: Stop, completion: (() -> Void)!) {
        moveToStops([stop], completion: completion)
    }
    
    func moveOut(stop: Stop, color: UIColor, completion: (() -> Void)!) {
        moveToStops([stop]) {
            self.explode(color, completion: completion)
        }
    }
    
    private func moveToStops(stops: [Stop], completion: (() -> Void)!) {
        var actions = [SKAction]()
        for stop in stops {
            let waitAction = SKAction.waitForDuration(stop.waitDuration)
            let moveAction = SKAction.moveTo(stop.location, duration: stop.duration)
            moveAction.timingMode = stop.timingMode
            actions.appendContentsOf([waitAction, moveAction])
        }
        
        node.runAction(SKAction.sequence(actions)) {
            completion?()
        }
    }
    
    func explode(color: UIColor,completion: (() -> Void)!) {
        let board = node.parent!
        let blastNode = NSKeyedUnarchiver.unarchiveObjectWithFile(blastPath) as! SKEmitterNode
        blastNode.particleColorSequence = nil
        blastNode.particleColorBlendFactor = 1.0
        blastNode.particleColor = color
        blastNode.zPosition = 1000
        blastNode.position = node.position
        board.addChild(blastNode)
        node.alpha = 0.0
        
        let seconds = CGFloat(blastNode.numParticlesToEmit) / blastNode.particleBirthRate +
            blastNode.particleLifetime + blastNode.particleLifetimeRange / 2.0
        
        let blastNode2 = NSKeyedUnarchiver.unarchiveObjectWithFile(blastPath2) as! SKEmitterNode
        blastNode2.particleColorSequence = nil
        blastNode2.particleColorBlendFactor = 1.0
        blastNode2.particleColor = color
        blastNode2.zPosition = 1000
        blastNode2.position = node.position
        board.addChild(blastNode2)
        
        let seconds2 = CGFloat(blastNode2.numParticlesToEmit) / blastNode2.particleBirthRate +
            blastNode2.particleLifetime + blastNode2.particleLifetimeRange / 2.0
        
        blastNode.runAction(SKAction.waitForDuration(NSTimeInterval(seconds))) {
            blastNode.removeFromParent()
        }
        
        blastNode2.runAction(SKAction.waitForDuration(NSTimeInterval(seconds2))) {
            blastNode2.removeFromParent()
            
            let delayTime = dispatch_time(DISPATCH_TIME_NOW,
                Int64(0.2 * Double(NSEC_PER_SEC)))
            
            dispatch_after(delayTime, dispatch_get_main_queue(), {
                completion?()
            })
        }
    }
}

