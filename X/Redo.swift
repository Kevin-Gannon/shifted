//
//  HelpView.swift
//  X
//
//  Created by Kevin Gannon on 12/31/15.
//  Copyright © 2015 Kevin Gannon. All rights reserved.
//

import UIKit

class Redo: UIView {
    
    init() {
        super.init(frame: CGRectMake(0, 0, 54, 51))
        
        backgroundColor = UIColor.clearColor()
    }

    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    override func drawRect(rect: CGRect) {
        let fillColor = UIColor.whiteColor()
        let bezierPath = UIBezierPath()
        bezierPath.moveToPoint(CGPointMake(53.1, 36.3))
        bezierPath.addCurveToPoint(CGPointMake(50.5, 35), controlPoint1: CGPointMake(52.8, 35.2), controlPoint2: CGPointMake(51.6, 34.6))
        bezierPath.addLineToPoint(CGPointMake(47.8, 35.9))
        bezierPath.addCurveToPoint(CGPointMake(50.3, 25), controlPoint1: CGPointMake(49.4, 32.5), controlPoint2: CGPointMake(50.3, 28.8))
        bezierPath.addCurveToPoint(CGPointMake(25.3, 0), controlPoint1: CGPointMake(50.3, 11.2), controlPoint2: CGPointMake(39.1, 0))
        bezierPath.addCurveToPoint(CGPointMake(0.3, 25), controlPoint1: CGPointMake(11.5, 0), controlPoint2: CGPointMake(0.3, 11.2))
        bezierPath.addCurveToPoint(CGPointMake(25.3, 50), controlPoint1: CGPointMake(0.3, 38.8), controlPoint2: CGPointMake(11.5, 50))
        bezierPath.addCurveToPoint(CGPointMake(27.3, 48), controlPoint1: CGPointMake(26.4, 50), controlPoint2: CGPointMake(27.3, 49.1))
        bezierPath.addCurveToPoint(CGPointMake(25.3, 46), controlPoint1: CGPointMake(27.3, 46.9), controlPoint2: CGPointMake(26.4, 46))
        bezierPath.addCurveToPoint(CGPointMake(4.4, 25.1), controlPoint1: CGPointMake(13.8, 46), controlPoint2: CGPointMake(4.4, 36.6))
        bezierPath.addCurveToPoint(CGPointMake(25.3, 4.2), controlPoint1: CGPointMake(4.4, 13.6), controlPoint2: CGPointMake(13.8, 4.2))
        bezierPath.addCurveToPoint(CGPointMake(46.2, 25.1), controlPoint1: CGPointMake(36.8, 4.2), controlPoint2: CGPointMake(46.2, 13.6))
        bezierPath.addCurveToPoint(CGPointMake(44.1, 34.2), controlPoint1: CGPointMake(46.2, 28.3), controlPoint2: CGPointMake(45.5, 31.4))
        bezierPath.addLineToPoint(CGPointMake(43.2, 31.4))
        bezierPath.addCurveToPoint(CGPointMake(40.6, 30.1), controlPoint1: CGPointMake(42.9, 30.3), controlPoint2: CGPointMake(41.7, 29.7))
        bezierPath.addCurveToPoint(CGPointMake(39.3, 32.7), controlPoint1: CGPointMake(39.5, 30.4), controlPoint2: CGPointMake(38.9, 31.6))
        bezierPath.addLineToPoint(CGPointMake(41.7, 40.1))
        bezierPath.addCurveToPoint(CGPointMake(43.6, 41.5), controlPoint1: CGPointMake(42, 41), controlPoint2: CGPointMake(42.8, 41.5))
        bezierPath.addCurveToPoint(CGPointMake(44.2, 41.4), controlPoint1: CGPointMake(43.8, 41.5), controlPoint2: CGPointMake(44, 41.5))
        bezierPath.addLineToPoint(CGPointMake(51.6, 39))
        bezierPath.addCurveToPoint(CGPointMake(53.1, 36.3), controlPoint1: CGPointMake(52.8, 38.6), controlPoint2: CGPointMake(53.4, 37.4))
        bezierPath.closePath()
        bezierPath.miterLimit = 4;
        
        fillColor.setFill()
        bezierPath.fill()
    }
}
