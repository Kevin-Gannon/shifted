//
//  LevelNavigationButton.swift
//  X
//
//  Created by Kevin Gannon on 1/24/16.
//  Copyright © 2016 Kevin Gannon. All rights reserved.
//

import UIKit

enum ArrowDirection {
    case Left
    case Right
}

protocol LevelNaviationDelegate: class {
    func previousLevelSelected()
    func nextLevelSelected()
}

class LevelNavigationButton: UIControl {
    
    private let inactiveAlpha: CGFloat = 0.3
    private let activeAlpha: CGFloat = 0.7
    private let boundsPadding: CGFloat = 50.0
    
    var disabled: Bool = false {
        didSet {
            updateState()
        }
    }
    
    init(direction: ArrowDirection) {
        let arrow = Arrow()
        arrow.userInteractionEnabled = false
        
        var bounds = arrow.bounds
        bounds.size.width += boundsPadding
        bounds.size.height += boundsPadding
        
        super.init(frame: bounds)
        
        alpha = activeAlpha
        
        backgroundColor = UIColor.clearColor()
        
        let rotation = direction == .Left ? CGFloat(M_PI) : 0.0
        
        transform = CGAffineTransformConcat(CGAffineTransformMakeScale(0.8, 0.8),
            CGAffineTransformMakeRotation(rotation))
        
        addSubview(arrow)
        
        arrow.center = self.center
        
        setupTouchEvents()
    }
    
    func onTouch() {
        alpha = inactiveAlpha
    }
    
    func onRelease() {
        UIView.animateWithDuration(0.5, delay: 0,
            options: .AllowUserInteraction, animations: {
                self.alpha = self.activeAlpha
                }, completion: nil)
    }

    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    //MARK: - Private
    private func setupTouchEvents() {
        addTarget(self, action: #selector(LevelNavigationButton.onTouch),
            forControlEvents: UIControlEvents.TouchDown)
        addTarget(self, action: #selector(LevelNavigationButton.onTouch),
            forControlEvents: UIControlEvents.TouchDragInside)

        addTarget(self, action: #selector(LevelNavigationButton.onRelease),
            forControlEvents: UIControlEvents.TouchDragExit)
        addTarget(self, action: #selector(LevelNavigationButton.onRelease),
            forControlEvents: UIControlEvents.TouchUpInside)
    }
    
    private func updateState() {
        if disabled { setStateDisabled() } else { setStateEnabled() }
    }
    
    private func setStateEnabled() {
        alpha = activeAlpha
        userInteractionEnabled = true
    }
    
    private func setStateDisabled() {
        alpha = inactiveAlpha
        userInteractionEnabled = false
    }
}
