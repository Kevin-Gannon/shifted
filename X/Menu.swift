//
//  Menu.swift
//  X
//
//  Created by Kevin Gannon on 9/13/15.
//  Copyright (c) 2015 Kevin Gannon. All rights reserved.
//

import UIKit
import SpriteKit

class Menu : UIView {
    private let sound: MenuButton
    private let rate: MenuButton
    private let redo: MenuButton
    private let closeArrow: UIView
    private let closedSpacing: CGFloat = 9.0
    private let openSpacing: CGFloat = 20.0
    private let animateYDistance: CGFloat = 55.0
    private let paddingInset: CGFloat = 45.0
    private let startScale: CGFloat = 0.12
    private let closeArrowSpacing: CGFloat
    
    private var setSoundSetting: (() -> Void)!
    
    weak var delegate: MenuDelegate?
    
    init(buttonSize: CGFloat) {
        let redoView = Redo()
        redoView.transform = CGAffineTransformMakeScale(0.5, 0.5)
        
        let star = Rate()
        star.transform = CGAffineTransformMakeScale(0.5, 0.5)
        
        closeArrow = Arrow(fillColor: UIColor.darkGrayColor())
        closeArrow.transform = CGAffineTransformConcat(
            CGAffineTransformMakeScale(0.375, 0.375),
            CGAffineTransformMakeRotation(CGFloat(M_PI_2)))
        closeArrow.alpha = 0.0
        closeArrow.userInteractionEnabled = false
        
        sound = MenuButton(buttonSize: buttonSize, startScale: startScale,
            view: VolumeView())
        
        rate = MenuButton(buttonSize: buttonSize, startScale: startScale,
            view: star)
        
        redo = MenuButton(buttonSize: buttonSize, startScale: startScale,
            view: redoView)
        
        closeArrowSpacing = buttonSize / 3.0
        
        super.init(frame: CGRectMake(0, 0, 3 * buttonSize * startScale + paddingInset,
            buttonSize * startScale + paddingInset))
        
        sound.center = center
        addSubview(sound)
        
        redo.center = center
        redo.frame.origin.x += closedSpacing
        addSubview(redo)
        
        rate.center = center
        rate.frame.origin.x -= closedSpacing
        addSubview(rate)
        
        closeArrow.center = center
        closeArrow.frame.origin.y -= closeArrowSpacing
        addSubview(closeArrow)
        
        let volumeTapped = UITapGestureRecognizer(
            target: self,
            action: #selector(Menu.volumeTapped))
        
        let rateTapped = UITapGestureRecognizer(
            target: self,
            action: #selector(Menu.rateTapped))
        
        let redoTapped = UITapGestureRecognizer(
            target: self,
            action: #selector(Menu.redoTapped))

        sound.addGestureRecognizer(volumeTapped)
        rate.addGestureRecognizer(rateTapped)
        redo.addGestureRecognizer(redoTapped)
        
        setSoundSetting = {[weak self] in self?.volumeOff() }
    }
    
    private func volumeOff() {
        delegate?.volumeOff()
        setSoundSetting = {[weak self] in self?.volumeOn() }
    }
    
    private func volumeOn() {
        delegate?.volumeOn()
        setSoundSetting = {[weak self] in self?.volumeOff() }
    }
    
    func volumeTapped() {
        if !sound.isEnabled { return }
        
        userInteractionEnabled = false
        disableMenuButtons()
        
        sound.tapped {
            self.userInteractionEnabled = true
            self.enableMenuButtons()
        }
        
        setSoundSetting()
    }
    
    func rateTapped() {
        if !rate.isEnabled { return }
        
        userInteractionEnabled = false
        disableMenuButtons()
        
        rate.tapped {
            self.userInteractionEnabled = true
            self.enableMenuButtons()
        }
        
        delegate?.rateSelected()
    }
    
    func redoTapped() {
        if !redo.isEnabled { return }
        
        userInteractionEnabled = false
        disableMenuButtons()

        let onCompletion = delegate?.redoSelected()
        redo.tapped { onCompletion?() }
    }
    
    override func hitTest(point: CGPoint, withEvent event: UIEvent?) -> UIView? {
        if sound.isActive() && CGRectContainsPoint(sound.frame, point) {
            return sound
        }
        
        if rate.isActive() && CGRectContainsPoint(rate.frame, point) {
            return rate
        }
        
        if redo.isActive() && CGRectContainsPoint(redo.frame, point) {
            return redo
        }
        
        return super.hitTest(point, withEvent: event)
    }
    
    func setRedoEnabled(enabled: Bool) {
        redo.isEnabled = enabled
    }

    func open() {
        sound.open(0, yOffset: -animateYDistance, delay: 0, completion: nil)
        rate.open(-rate.bounds.size.width * 1.6, yOffset: -animateYDistance,
            delay: 0.09, completion: nil)
        redo.open(redo.bounds.size.width * 1.6, yOffset: -animateYDistance,
            delay: 0.09, completion: nil)
        
        animateCloseArrowOpen()
    }
    
    func close() {
        rate.close(rate.bounds.size.width * 1.6, yOffset: animateYDistance,
            delay: 0, completion: nil)
        redo.close(-redo.bounds.size.width * 1.6, yOffset: animateYDistance,
            delay: 0, completion: nil)
        sound.close(0, yOffset: animateYDistance, delay: 0.06) {
            self.userInteractionEnabled = true
            self.enableMenuButtons()
        }
        
        animateCloseArrowClosed()
    }
    
    private func animateCloseArrowOpen() {
        UIView.animateWithDuration(0.83, delay: 0.15, usingSpringWithDamping: 0.43,
            initialSpringVelocity: 0.3, options: .CurveEaseOut, animations: {
                self.closeArrow.alpha = 0.7
                self.closeArrow.frame.origin.y += self.closeArrowSpacing
            }, completion: nil)
    }
    
    private func animateCloseArrowClosed() {
        UIView.animateWithDuration(0.15, animations: {
            self.closeArrow.alpha = 0.0
            self.closeArrow.frame.origin.y += 15.0
            self.closeArrow.transform = CGAffineTransformConcat(
                CGAffineTransformMakeScale(0.2, 0.2),
                CGAffineTransformMakeRotation(CGFloat(M_PI_2)))
            }, completion: {completed in
                self.closeArrow.transform = CGAffineTransformConcat(
                    CGAffineTransformMakeScale(0.375, 0.375),
                    CGAffineTransformMakeRotation(CGFloat(M_PI_2)))
                self.closeArrow.frame.origin.y -= self.closeArrowSpacing + 15.0
        })
    }
    
    private func disableMenuButtons() {
        sound.isEnabled = false
        rate.isEnabled = false
        redo.isEnabled = false
    }
    
    private func enableMenuButtons() {
        sound.isEnabled = true
        rate.isEnabled = true
        redo.isEnabled = true
    }
    
    required init(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}
