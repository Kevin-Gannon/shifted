//
//  Level.swift
//  X
//
//  Created by Kevin Gannon on 7/5/15.
//  Copyright © 2015 Kevin Gannon. All rights reserved.
//
import UIKit

struct Level {
    var id: Int
    var dimension: Int
    var gaps: [Gap]
    var board: [Tile]
    var boardData: GameBoardData
}

enum LevelColor: String {
    case Red
    case Blue
    case Yellow
    case Dark
    case Orange
    case Purple
    case Green
}

struct Gap {
    var index: Int
    var direction: Direction
}
