//
//  MoveCommand.swift
//  X
//
//  Created by Kevin Gannon on 7/16/15.
//  Copyright © 2015 Kevin Gannon. All rights reserved.
//

import UIKit

enum Direction: String {
    case Up
    case Down
    case Left
    case Right
}

enum MoveOrder {
    case None
    case DeferedBombTick(source: Int, value: Int, coords: [(Int,Int)])
    case MoveOrder(source: Int, destination: Int)
    case PushOrder(source: Int, delay: Int, destination: Int, pusherDistance: Int)
    case MoveOut(source: Int, distance: Int, exitedGap: Bool)
    case MoveOutPush(source: Int, delay: Int, pusherDistance: Int)
    case BombTick(source: Int, value: Int)
}



