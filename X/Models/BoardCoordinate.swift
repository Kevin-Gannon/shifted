//
//  BoardCoordinate.swift
//  X
//
//  Created by Kevin Gannon on 7/14/15.
//  Copyright © 2015 Kevin Gannon. All rights reserved.
//

import UIKit

struct BoardCoordinate : Hashable {
    var row: Int
    var col: Int
    
    init(row: Int, col: Int) {
        self.row = row
        self.col = col
    }
    
    var hashValue: Int {
        return (31 &* row.hashValue) &+ col.hashValue
    }
}

func == (lhs: BoardCoordinate, rhs: BoardCoordinate) -> Bool {
    return (lhs.row.hashValue == rhs.row.hashValue) &&
        (lhs.col.hashValue == rhs.col.hashValue)
}
