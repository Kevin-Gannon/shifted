//
//  GameBoardData.swift
//  X
//
//  Created by Kevin Gannon on 7/11/15.
//  Copyright © 2015 Kevin Gannon. All rights reserved.
//

import UIKit
import SpriteKit

struct GameBoardData {
    var boardWidth: CGFloat
    var tileWidth: CGFloat
    var padding: CGFloat
    var textures: Dictionary<TextureType, SKTexture>
    
    func textureFromType(type: TextureType) -> SKTexture {
        return textures[type]!
    }
}

enum TextureType: String {
    case Pause
    case Border
    case Empty
    case Bomb
    case Gap
    case Left
    case Right
    case Up
    case Down
}

enum GameState {
    case Active
    case TutorialActive(solution: [Direction], index: Int)
    case TutorialInactive(solution: [Direction], index: Int)
    case Over
    case Completed
    case Paused
    case TransitionPaused
    case TransitionActive
}
