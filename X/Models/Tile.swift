//
//  Tile.swift
//  X
//
//  Created by Kevin Gannon on 7/14/15.
//  Copyright © 2015 Kevin Gannon. All rights reserved.
//

import UIKit
import SpriteKit

enum TileType : String {
    case Mover
    case Sitter
    case Empty
    case Bomb
}

protocol Tile {
    var type: TileType { get }
}

struct Mover : Tile {
    var direction: Direction
    var type: TileType = .Mover
    
    init(direction: Direction) {
        self.direction = direction
    }
}

struct Bomb : Tile {
    var type: TileType = .Bomb
    var value: Int
    
    init(value: Int) {
        self.value = value
    }
}

struct Sitter: Tile {
    var type: TileType = .Sitter
}

struct Empty: Tile {
    var type: TileType = .Empty
}
