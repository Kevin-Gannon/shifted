//
//  Game.swift
//  X
//
//  Created by Kevin Gannon on 7/14/15.
//  Copyright © 2015 Kevin Gannon. All rights reserved.
//

import UIKit

class GameModel {
    
    private var queue = [Direction]()
    private var gameBoard: [Tile]
    private var gaps: [Gap]
    private var queueTimer = NSTimer()
    private var bombCount: Int = 0
    
    private let dimension: Int
    private let queueDelay = 0.35
    
    weak var delegate: GameProtocol?
    
    init(dimension: Int, tiles: [Tile], gaps: [Gap]) {
        self.dimension = dimension
        self.gameBoard = tiles
        self.gaps = gaps
        
        setBombCount()
    }
    
    func slide(direction: Direction) {
        queue.append(direction)
        if !queueTimer.valid {
            executeMoveCommand(queueTimer)
        }
    }
    
    @objc func executeMoveCommand(timer: NSTimer) {
        if queue.count == 0 { return }
        
        while queue.count > 0 {
            let command = queue[0]
            queue.removeAtIndex(0)
            performSlide(command)
        }
        
        setQueueTimer()
    }
    
    func resetLevel(tiles: [Tile], gaps: [Gap]) {
        self.gameBoard = tiles
        self.gaps = gaps
        
        queueTimer.invalidate()
        queue = [Direction]()
        
        setBombCount()
    }
    
    private func setQueueTimer() {
        queueTimer = NSTimer.scheduledTimerWithTimeInterval(
            queueDelay,
            target: self,
            selector: #selector(GameModel.executeMoveCommand(_:)),
            userInfo: nil, repeats: false)
    }
    
    private func getFromGameboard(row row: Int, col: Int) -> Tile {
        return gameBoard[row * dimension + col]
    }
    
    private func setOnGameboard(row row: Int, col: Int, tile: Tile) {
        gameBoard[row * dimension + col] = tile
    }
    
    private func coordinateGenerator(
        direction: Direction, iteration: Int) -> [(Int,Int)] {
        
        var buffer = [(Int,Int)](count:dimension, repeatedValue: (0,0))
        for i in 0..<self.dimension {
            switch direction {
            case .Down: buffer[i] = (i,iteration)
            case .Up: buffer[i] = (dimension - i - 1, iteration)
            case .Left: buffer[i] = (iteration,i)
            case .Right: buffer[i] = (iteration,dimension - i - 1)
            }
        }
        
        return buffer
    }
    
    private func performSlide(direction: Direction) {
        var deferred = [MoveOrder]()
        var bombShouldTick = false
        
        func tryBombTick() {
            for order in deferred {
                switch order {
                case let .DeferedBombTick(source: s, value: v, coords: c):
                    updateTilePositions(s, destination: s, coords: c)
                    delegate?.bombTick(c[s], levelLost: v == 0 ? true : false)
                default:
                    break
                }
            }
            
            bombShouldTick = true
            deferred = [MoveOrder]()
        }
        
        for i in 0..<dimension {
            let coords = coordinateGenerator(direction, iteration: i)
            let tiles = coords.map() { (coordinate: (Int,Int)) -> Tile in
                let (x,y) = coordinate
                return self.getFromGameboard(row: x, col: y)
            }
            
            let orders = getOrders(tiles, direction: direction, gapIndex: i)
            
            for order in orders {
                
                switch order {
                    
                case let .MoveOrder(source: s, destination: d):
                    updateTilePositions(s, destination: d, coords: coords)
                    delegate?.moveTile(coords[s], to: coords[d])
                    tryBombTick()
                    
                case let .PushOrder(source: s, delay: de, destination: d, pusherDistance: p):
                    updateTilePositions(s, destination: d, coords: coords)
                    var levelLost = false
                    let (sourceRow,sourceCol) = coords[d]
                    if let bomb = getFromGameboard(row: sourceRow, col: sourceCol)
                        as? Bomb where bomb.value == 0 { levelLost = true }
                    delegate?.pushTile(coords[s],to: coords[d],delay: de,
                                       pusherDistance: p,levelLost: levelLost)
                    
                case let .MoveOut(source: s, distance: di, exitedGap: eg):
                    updateTilePositions(s, destination: nil, coords: coords)
                    
                    if eg { bombCount -= 1 }
                    delegate?.moveOut(coords[s], direction: direction,
                                      distance: di, levelCompleted: bombCount == 0 && eg)
                    tryBombTick()
                    
                case let .MoveOutPush(source: s, delay: de, pusherDistance: p):
                    updateTilePositions(s, destination: nil, coords: coords)
                    delegate?.moveOutPush(coords[s], direction: direction, pusherDistance: p, delay: de)
                    
                case let .BombTick(source: s, value: v):
                    if bombShouldTick {
                        updateTilePositions(s, destination: s, coords: coords)
                        delegate?.bombTick(coords[s], levelLost: v == 0 ? true : false)
                        break
                    }
                    
                    deferred.append(.DeferedBombTick(source: s, value: v, coords: coords))

                default:
                    break
                }
            }
        }
    }
    
    private func updateTilePositions(source: Int, destination: Int?, coords: [(Int,Int)]) {
        let (sourceRow,sourceCol) = coords[source]
        var tile = getFromGameboard(row: sourceRow, col: sourceCol)
        if var bomb = tile as? Bomb {
            bomb.value -= 1
            tile = bomb
        }
        setOnGameboard(row: sourceRow, col: sourceCol, tile: Empty())
        
        if let dest = destination {
            let (destinationRow,destinationCol) = coords[dest]
            setOnGameboard(row: destinationRow, col: destinationCol, tile: tile)
        }
    }
    
    private func setBombCount() {
        self.bombCount = gameBoard.reduce(0, combine: { bombs, tile in
            if tile is Bomb { return bombs + 1 }
            return bombs
        })
    }
    
    private func isAlignedWithGap(direction: Direction, gapIndex: Int) -> Bool {
        for gap in gaps {
            if gap.direction == direction && gapIndex == gap.index { return true }
        }
        
        return false
    }
    
    private func getOrders(
        group: [Tile],
        direction: Direction,
        gapIndex: Int) -> [MoveOrder]  {
        
        var moveBuffer = [MoveOrder]()
        var openIndex = isAlignedWithGap(direction, gapIndex: gapIndex) ? -1 : 0
        var pushList = [Int]()
        var bombIndex = -1
        
        func emptyPushList(triggerBombTick: Bool = true) {
            if triggerBombTick {
                for pushedIndex in pushList {
                    switch group[pushedIndex].type {
                    case .Bomb:
                        let bomb = group[pushedIndex] as! Bomb
                        moveBuffer.append(.BombTick(
                            source: pushedIndex,
                            value: bomb.value - 1))
                    default:
                        break
                    }
                }
            }
            
            pushList = [Int]()
        }
        
        for (groupIndex,tile) in group.enumerate() {
            switch tile.type {
            case .Mover:
                if (tile as! Mover).direction != direction {
                    pushList.append(groupIndex)
                    continue
                }
                
                if groupIndex == openIndex {
                    openIndex = openIndex == -1 ? -1 : openIndex + 1
                    continue
                }
                
                let distance = openIndex == -1 ? groupIndex + 1 : groupIndex -
                    openIndex - pushList.count
                
                for (index,pushedIndex) in pushList.enumerate() {
                    if pushedIndex == openIndex {
                        
                        switch group[pushedIndex].type {
                        case .Bomb:
                            let bomb = group[pushedIndex] as! Bomb
                            moveBuffer.append(.BombTick(
                                source: pushedIndex,
                                value: bomb.value - 1))
                        default:
                            break
                        }
                        
                        openIndex = openIndex == -1 ? -1 : openIndex + 1
                        continue
                    }
                    
                    let delay = groupIndex - pushedIndex - 1 -
                        (pushList.count - 1 - index)
                    
                    if openIndex == -1 {
                        moveBuffer.append(.MoveOutPush(
                            source: pushedIndex,
                            delay: delay,
                            pusherDistance: distance))
                    }
                        
                    else {
                        moveBuffer.append(.PushOrder(
                            source: pushedIndex,
                            delay: delay,
                            destination: openIndex,
                            pusherDistance: distance))
                    }
                    
                    openIndex = openIndex == -1 ? -1 : openIndex + 1
                }
                
                if openIndex == -1 {
                    moveBuffer = moveBuffer.map({order in
                        switch order {
                        case let .MoveOutPush(source: s, delay: de, pusherDistance: _):
                            return .MoveOutPush(source: s,
                                delay: de,
                                pusherDistance: distance)
                            
                        case let .MoveOut(source: s, distance: _, exitedGap: eg):
                            return .MoveOut(
                                source: s,
                                distance: distance,
                                exitedGap: eg)
                        default:
                            return order
                        }
                    })
                    
                    if bombIndex == -1 {
                        moveBuffer.append(.MoveOut(
                            source: groupIndex,
                            distance: distance,
                            exitedGap: false))
                    }
                    else {
                        moveBuffer.append(.MoveOut(
                            source: groupIndex,
                            distance: distance,
                            exitedGap: true))
                        
                        bombIndex = -1
                    }
                }
                    
                else if groupIndex != openIndex {
                    moveBuffer.append(.MoveOrder(
                        source: groupIndex,
                        destination: openIndex))
                }
                
                emptyPushList(false)
                
                openIndex = openIndex == -1 ? -1 : openIndex + 1
            
            case .Sitter:
                openIndex = groupIndex + 1
                emptyPushList()
                
            case .Bomb:
                pushList.append(groupIndex)
                bombIndex = groupIndex
                
            case .Empty:
                break
            }
        }
        
        emptyPushList()
        
        return moveBuffer
    }
}
