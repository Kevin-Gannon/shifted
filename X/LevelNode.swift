//
//  LevelNode.swift
//  X
//
//  Created by Kevin Gannon on 8/19/15.
//  Copyright (c) 2015 Kevin Gannon. All rights reserved.
//

import UIKit
import SpriteKit

class LevelNode : SKNode {
    
    var tiles: Dictionary<BoardCoordinate,TileNodeProtocol> = [:]
    var insideNodes = Set<SKNode>()
    var outsideNodes = Set<SKNode>()
    var boardData: GameBoardData
    var border: SKSpriteNode
    var level: Level
    var board: SKSpriteNode
    var gaps: [SKSpriteNode] = [SKSpriteNode]()
    
    private let slideDuration = 0.35
    private let slideOutDuration = 0.36
    
    init(level: Level) {
        self.boardData = level.boardData
        self.level = level
    
        let bounds = UIScreen.mainScreen().bounds
        
        border = SKSpriteNode(texture: boardData.textureFromType(.Border))
        border.position = CGPointMake(bounds.size.width / 2.0,bounds.size.height / 2.0)
        border.zPosition = 1.0
        
        board = SKSpriteNode()
        
        super.init()
        
        for row in 0..<level.dimension {
            for col in 0..<level.dimension {
                let coordinate = BoardCoordinate(row: row, col: col)
                let tile = createTile(coordinate, type: .Empty)
                board.addChild(tile.node)
                groupTile(coordinate,tile: tile.node)
            }
        }
        
        let zeroX = bounds.size.width / 2.0 - boardData.boardWidth / 2.0
            + boardData.tileWidth / 2.0
        let zeroY = bounds.size.height / 2.0 - boardData.boardWidth / 2.0
            + boardData.tileWidth / 2.0
        
        board.position = CGPointMake(zeroX,zeroY)
    
        addChild(border)
        addChild(board)
        
        board.zPosition = 2
        
        addGapNodes()
    }

    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    func addTile(tile: Tile, coordinate: BoardCoordinate) {
        switch tile.type {
        case .Mover:
            guard let tile = tile as? Mover else { return }
            let textureType = TextureType(rawValue: tile.direction.rawValue)!
            let mover = MoverTile(tile: createTile(coordinate, type:
                textureType), direction: tile.direction)
            tiles[coordinate] = mover
            groupTile(coordinate, tile: mover.node)
            mover.node.position = getTilePosition(coordinate)
            board.addChild(mover.node)
        case .Bomb:
            guard let tile  = tile as? Bomb else { return }
            let bomb = BombNode(tile: createTile(coordinate, type: .Bomb), bombValue: tile.value)
            tiles[coordinate] = bomb
            groupTile(coordinate, tile: bomb.node)
            bomb.node.position = getTilePosition(coordinate)
            board.addChild(bomb.node)
        default:
            break
        }
    }
    
    func revealGaps(completion: (() -> Void)!) {
        for (index,gap) in gaps.enumerate() {
            let action = SKAction.scaleXTo(1.0, duration: 0.40)
            action.timingMode = .EaseOut
            gap.runAction(action) {
                if index == self.gaps.count - 1 { completion?() }
            }
        }
    }
    
    func moveOut(
        fromKey: BoardCoordinate,
        direction: Direction,
        distance: Int,
        delay: Int,
        completion: (() -> Void)!) {
            
        let tile = tiles[fromKey]!
        
        var position: CGPoint
        var distanceToEdge: Int
        
        switch direction {
        case .Up:
            let coor = BoardCoordinate(row: level.dimension - 1, col: fromKey.col)
            distanceToEdge = level.dimension - 1 - fromKey.row
            position = getTilePosition(coor)
            position.y += 10 * (boardData.padding + boardData.tileWidth)
        case .Down:
            let coor = BoardCoordinate(row: 0, col: fromKey.col)
            distanceToEdge = fromKey.row
            position = getTilePosition(coor)
            position.y -= 10 * (boardData.padding + boardData.tileWidth)
        case .Right:
            let coor = BoardCoordinate(row: fromKey.row, col: level.dimension - 1)
            distanceToEdge = level.dimension - 1 - fromKey.col
            position = getTilePosition(coor)
            position.x += 10 * (boardData.padding + boardData.tileWidth)
        case .Left:
            let coor = BoardCoordinate(row: fromKey.row, col: 0)
            distanceToEdge = fromKey.col
            position = getTilePosition(coor)
            position.x -= 10 * (boardData.padding + boardData.tileWidth)
        }
        
        let perSquareDuration = slideOutDuration / Double(level.dimension)
        let duration = (perSquareDuration * (Double(10 + distanceToEdge)))
        let waitDuration = perSquareDuration * Double(delay)
        
        tile.move(Stop(wait: waitDuration, location: position,
            duration: duration)) { completion?() }
    }
    
    private func getStop(
        fromKey: BoardCoordinate,
        toKey: BoardCoordinate,
        delay: Int,
        pusherDistance: Int) -> Stop {
            
        let perSquareDuration = slideDuration / Double(pusherDistance)
        let waitDuration = perSquareDuration * Double(delay)
        
        return Stop(wait: waitDuration, location: getTilePosition(toKey),
            duration: slideDuration - waitDuration)
    }
    
    func setFocusToTilesWithDirection(
        direction: Direction,
        withBomb: Bool = false) {
            
        setFocusOfTilesWithDirection(direction, withBomb: withBomb, focus: 100)
    }
    
    func resetFocusOfTilesWithDirection(
        direction: Direction,
        withBomb: Bool = false) {
            
        setFocusOfTilesWithDirection(direction, withBomb: withBomb, focus: 4)
    }
    
    func moveTile(
        fromKey: BoardCoordinate,
        toKey: BoardCoordinate,
        delay: Int = 0,
        pusherDistance: Int = 1,
        completion: (() -> Void)! = nil) {
            
        let tile = tiles[fromKey]!
        
        tiles.removeValueForKey(fromKey)
        tiles[toKey] = tile
        
        tile.move(getStop(
            fromKey,
            toKey: toKey,
            delay: delay,
            pusherDistance: pusherDistance)) { completion?() }
    }
    
    func bombTick(fromKey: BoardCoordinate, completion: (() -> Void)!) {
        let bomb = tiles[fromKey]! as! BombNode
        bomb.tick(slideDuration) { completion?() }
    }
    
    func resetTilesToStartingPositions(resetGap: Bool = true) {
        if resetGap { gaps.forEach({ gap in gap.xScale = 0.0 })}
        
        let values = tiles.values
        tiles.removeAll(keepCapacity: true)
        
        for value in values {
            tiles[value.startingCoordinate] = value
        }
        
        for (index,tile) in level.board.enumerate() {
            if tile.type == .Empty { continue }
            let row = index / level.dimension
            let col = index % level.dimension
            let coordinate = BoardCoordinate(row: row, col: col)
            
            guard let tileProtocol = tiles[coordinate] else {
                addTile(tile, coordinate: coordinate)
                continue
            }
            
            tileProtocol.resetToPosition(getTilePosition(coordinate))
        }
    }
    
    private func createTile(coordinate: BoardCoordinate, type: TextureType) -> TileNode {
        let tile = TileNode(startingCoordinate: coordinate,
            texture: boardData.textureFromType(type))
        
        tile.node.position = getTilePosition(coordinate)
        
        return tile
    }
    
    private func groupTile(coor: BoardCoordinate, tile: SKNode) {
        tile.alpha = 0.0
        
        if coor.col == 0 || coor.col == level.dimension - 1
            || coor.row == 0 || coor.row == level.dimension - 1 {
                outsideNodes.insert(tile)
                return
        }
        
        insideNodes.insert(tile)
    }
    
    private func getTilePosition(coordinate: BoardCoordinate) -> CGPoint {
        return CGPointMake(CGFloat(coordinate.col) * (boardData.tileWidth + boardData.padding),
            CGFloat(coordinate.row) * (boardData.tileWidth + boardData.padding))
    }
    
    private func addGapNodes() {
        for gap in level.gaps {
            var coordinate: BoardCoordinate
            var startingPoint: CGPoint
            var rotation: CGFloat = 0.0
            
            let gapNode = SKSpriteNode(texture: boardData.textureFromType(.Gap))

            switch gap.direction {
            case .Up:
                let row = level.dimension - 1
                let col = gap.index
                coordinate = BoardCoordinate(row: row, col: col)
                startingPoint = getTilePosition(coordinate)
                startingPoint.x += board.position.x
                startingPoint.y += board.position.y + boardData.tileWidth
                
            case .Down:
                let row = 0
                let col = gap.index
                coordinate = BoardCoordinate(row: row, col: col)
                startingPoint = getTilePosition(coordinate)
                startingPoint.x += board.position.x
                startingPoint.y += board.position.y - boardData.tileWidth
                
            case .Left:
                let col = 0
                let row = gap.index
                coordinate = BoardCoordinate(row: row, col: col)
                startingPoint = getTilePosition(coordinate)
                startingPoint.x += board.position.x - boardData.tileWidth
                startingPoint.y += board.position.y
                rotation = CGFloat(M_PI / 2.0)
                
            case .Right:
                let col = level.dimension - 1
                let row = gap.index
                coordinate = BoardCoordinate(row: row, col: col)
                startingPoint = getTilePosition(coordinate)
                startingPoint.x += board.position.x + boardData.tileWidth
                startingPoint.y += board.position.y
                rotation = CGFloat(M_PI / 2.0)
            }
            
            gapNode.xScale = 0.0
            gapNode.position = CGPointMake(startingPoint.x, startingPoint.y)
            gapNode.zRotation = rotation
            gapNode.zPosition = 3
            
            addChild(gapNode)
            
            gaps.append(gapNode)
        }
    }
    
    private func setFocusOfTilesWithDirection(
        direction: Direction,
        withBomb: Bool = false,
        focus: CGFloat) {
            
        let filteredTiles = tiles.values.filter({tile in
            guard let mover = tile as? MoverTile else {
                
                if withBomb && (tile as? BombNode) != nil { return true }
                
                return false
            }
            return mover.direction == direction
        })
        
        for tile in filteredTiles {
            tile.node.zPosition = focus
        }
    }
}
